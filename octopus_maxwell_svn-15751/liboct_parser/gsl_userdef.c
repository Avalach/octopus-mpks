/*
 Copyright (C) 2002 M. Marques, A. Castro, A. Rubio, G. Bertsch

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

 $Id: gsl_userdef.c 14262 2015-06-12 17:49:58Z dstrubbe $
*/

#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_laguerre.h>
#include <gsl/gsl_sf_hyperg.h>
#include <math.h>
#include <stdio.h>


/* ------------------------------------------------------ */
gsl_complex gsl_complex_step_real (gsl_complex a)
{        
  gsl_complex z;
	
  if (GSL_REAL(a) < 0)
    {
      GSL_SET_COMPLEX (&z, 0, 0);
    }
  else
    {
      GSL_SET_COMPLEX (&z, 1, 0);
    }
  
  return z;
}


/* ------------------------------------------------------ */
gsl_complex gsl_complex_min_real (gsl_complex a, gsl_complex b)
{
  gsl_complex z;
  double min;
	
  /* just consider real parts */
  min = GSL_REAL(a) < GSL_REAL(b) ? GSL_REAL(a) : GSL_REAL(b);
  GSL_SET_COMPLEX (&z, min, 0);
	
  return z;
}


/* ------------------------------------------------------ */
gsl_complex gsl_complex_max_real (gsl_complex a, gsl_complex b)
{
  gsl_complex z;
  double max;
	
  /* just consider real parts */
  max = GSL_REAL(a) > GSL_REAL(b) ? GSL_REAL(a) : GSL_REAL(b);
  GSL_SET_COMPLEX (&z, max, 0);
	
  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_carg (gsl_complex a)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_complex_arg(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_cabs (gsl_complex a)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_complex_abs(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_cabs2 (gsl_complex a)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_complex_abs2(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_clogabs (gsl_complex a)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_complex_logabs(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_erf (gsl_complex a)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_sf_erf(GSL_REAL(a)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_arctan2 (gsl_complex a, gsl_complex b)
{        
  gsl_complex z, p;

  if(GSL_REAL(b) != 0.0)
    {
      z = gsl_complex_arctan(gsl_complex_div(a, b));
      if(GSL_REAL(b) < 0.0){
	GSL_SET_COMPLEX (&p, M_PI, 0);
	if(GSL_REAL(a) >= 0.0)
	  z = gsl_complex_add(z, p);
	else
	  z = gsl_complex_sub(z, p);
      }
    }
  else
    {
      if(GSL_REAL(a) >= 0.0)
	{
	  GSL_SET_COMPLEX (&z, M_PI/2.0, 0.0);
	}
      else
	{
	  GSL_SET_COMPLEX (&z, -M_PI/2.0, 0.0);
	}
    }

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_realpart (gsl_complex a)
{
  gsl_complex z;

  GSL_SET_COMPLEX (&z, GSL_REAL(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_imagpart (gsl_complex a)
{
  gsl_complex z;

  GSL_SET_COMPLEX (&z, GSL_IMAG(a), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_round (gsl_complex a)
{
  gsl_complex z;

  GSL_SET_COMPLEX (&z, trunc(GSL_REAL(a)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_ceiling (gsl_complex a)
{
  gsl_complex z;

  GSL_SET_COMPLEX (&z, ceil(GSL_REAL(a)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_floor (gsl_complex a)
{
  gsl_complex z;

  GSL_SET_COMPLEX (&z, floor(GSL_REAL(a)), 0);

  return z;
}

/* ------------------------------------------------------ */
double gsl_userdef_factorial(int nn)
{
  int ii;
  double fact = 1.0;
    
  if (nn == 0 || nn == 1) {
    fact = 1.0;
  } else {
    for (ii = nn; ii > 1; ii--) 
      fact *= (double) ii;
  }
    
  return fact;
}

/* ------------------------------------------------------ */
double gsl_userdef_ratio_factorial(int n1, int n2)
{
  int ii;
  double rfact = 1.0;
    
  if (n1 == n2) {
    return 1.0;
  } else if (n2 > n1) {
    return 0.0;
  } else if (n2 == 0 || n2 == 1) {
    return gsl_userdef_factorial(n1);
  } else {
    for (ii = n2 + 1; ii <= n1; ii++) rfact *= (double) ii;
  }
    
  return rfact;
}

/* ------------------------------------------------------ */
double gsl_userdef_hermite_polynomial(int n1, double xx)
{
  int nn, mm;
  double hp = 1.0;
    
  if (n1 % 2 == 1) {
    if (n1 == 1) return 2.0 * xx;
    if (n1 == 3) return 4.0 * xx * (2.0 * xx * xx - 3.0);
    nn = (n1 - 1) / 2;
    for (mm = nn; mm > 0; mm--) {
      hp = 1.0 - 2.0 * (double) (nn - mm + 1) / (double) (mm * (2 * mm + 1)) * xx * xx * hp;
    }
      hp *= 2.0 * xx * gsl_userdef_ratio_factorial(2 * nn + 1, nn);
    } else {
      if (n1 == 0) return 1.0;
      if (n1 == 2) return 2.0 * (2.0 * xx * xx - 1.0);
      nn = n1 / 2;
      for (mm = nn; mm > 0; mm--) {
        hp = 1.0 - 2.0 * (double) (nn - mm + 1) / (double) (mm * (2 * mm - 1)) * xx * xx * hp;
      }
      hp *= gsl_userdef_ratio_factorial(2 * nn, nn);
    }
  if (nn % 2 == 1) hp *= -1.0;

  return hp;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_hermite_n (gsl_complex nn, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_userdef_hermite_polynomial(GSL_REAL(nn),GSL_REAL(xx)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_hermite_hyperg_U_n (gsl_complex nn, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, pow(2.0, GSL_REAL(nn))*gsl_sf_hyperg_U(-GSL_REAL(nn)/2.0, 0.5, GSL_REAL(xx)*GSL_REAL(xx)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_laguerre_1 (gsl_complex aa, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_sf_laguerre_1(GSL_REAL(aa), GSL_REAL(xx)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_laguerre_2 (gsl_complex aa, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_sf_laguerre_2(GSL_REAL(aa), GSL_REAL(xx)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_laguerre_3 (gsl_complex aa, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_sf_laguerre_3(GSL_REAL(aa), GSL_REAL(xx)), 0);

  return z;
}

/* ------------------------------------------------------ */
gsl_complex gsl_complex_laguerre_n (gsl_complex nn, gsl_complex aa, gsl_complex xx)
{        
  gsl_complex z;

  GSL_SET_COMPLEX (&z, gsl_sf_laguerre_n((int) GSL_REAL(nn), GSL_REAL(aa), GSL_REAL(xx)), 0);

  return z;
}
