

###################################################################################################################################
# Maxwell states routines 
#


# Initializing Maxwell states
# File: states/states.F90

	subroutine maxwell_states_init(maxwell_st, maxwell_gr, maxwell_geo, maxwell_mc)


# Allocate Maxwell states
# File: states/states.F90

	subroutine maxwell_states_allocate(maxwell_st, maxwell_mesh)


# Building a RS element
# File: states/states.F90

	subroutine maxwell_build_rs_element(e_element, b_element, rs_sign, rs_element, ep_element, mu_element)


# Building a RS vector
# File: states/states.F90

	subroutine maxwell_build_rs_vector(e_vector, b_vector, rs_sign, rs_vector, ep_element, mu_element)


# Building a RS state
# File: states/states.F90

	subroutine maxwell_build_rs_state(e_field, b_field, rs_sign, rs_state, ep_field, mu_field, np)


# Building a RS current element
# File: states/states.F90

	subroutine maxwell_build_rs_current_element(current_element, rs_current_element, ep_element)


# Building a RS current vector
# File: states/states.F90

	subroutine maxwell_build_rs_current_vector(current_vector, rs_current_vector, ep_element)


# Building a RS current state
# File: states/states.F90

	subroutine maxwell_build_rs_current_state(current_state, rs_current_state, ep_field, np)


# Get electric field element from RS element
# File: states/states.F90

	subroutine maxwell_get_electric_field_element(rs_state_element, e_element, ep_element)


# Get magnetic field element from RS element
# File: states/states.F90

	subroutine maxwell_get_magnetic_field_element(rs_state_element, rs_sign, b_element, mu_element)


# Get electric field vector from RS vector
# File: states/states.F90

	subroutine maxwell_get_electric_field_vector(rs_state_vector, electric_field_vector, ep_element)


# Get magnetic field vector from RS vector
# File: states/states.F90

	subroutine maxwell_get_magnetic_field_vector(rs_state_vector, rs_sign, magnetic_field_vector, mu_element)


# Get electric field state from RS state
# File: states/states.F90

	subroutine maxwell_get_electric_field_state(rs_state, electric_field, ep_field, np)


# Get magnetic field state from RS state
# File: states/states.F90

	subroutine maxwell_get_magnetic_field_state(rs_state, rs_sign, magnetic_field, mu_field, np)


# Get current density element from RS current density element
# File: states/states.F90

	subroutine maxwell_get_current_element(rs_current_element, current_element, ep_element)


# Get current density vector from RS current density vector
# File: states/states.F90

	subroutine maxwell_get_current_vector(rs_current_vector, current_vector, ep_element)


# Get current density state from RS current density state
# File: states/states.F90

	subroutine maxwell_get_current_state(rs_current_field, current_field, ep_field, np)


# Calculate difference between predicted and corrected Maxwell state
# File: states/states.F90

	subroutine maxwell_state_diff(rs_state_old, rs_state, maxwell_gr, diff)






###################################################################################################################################
# Maxwell td run routines 
#


# Initializing Maxwell td variables
# File: td/td.F90

	subroutine maxwell_td_init(td, maxwell_sys, maxwell_hm, maxwell_st, hm)

# Expansion of subroutine td_run() for Maxwell propagation
# File: td/td.F90

	subroutine td_run(sys, hm, fromScratch, maxwell_sys, maxwell_hm)

# Subroutine for a free Maxwell simulation (Maybe this can be merged in one routine with td_run())
# File: td/td.F90

	subroutine td_run_maxwell_free(maxwell_sys, maxwell_hm, fromScratch)


# Maxwell restart routine for dumping the data
# File: td/td.F90

	subroutine maxwell_td_dump(maxwell_restart, maxwell_gr, maxwell_st, maxwell_hm, td, iter, ierr)


# Maxwell restart routine for loading the data
# File: td/td.F90

	subroutine maxwell_td_load(maxwell_restart, maxwell_gr, maxwell_st, maxwell_hm, td, ierr)






###################################################################################################################################
# Maxwell and matter propagator routines 
#


# Expansion of subroutine propagator_dt() for Maxwell propagation
# File: td/propagator.F90

	subroutine propagator_dt(ks, hm, gr, st, tr, time, dt, ionic_scale, nt, ions, geo, &
    				 scsteps, update_energy, qcchi, maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, mx_iter_steps)


# Expansion of subroutine td_etrs_sc() for Maxwell propagation
# File: td/propagator_etrs.F90

	subroutine td_etrs_sc(ks, hm, gr, st, tr, time, dt, ionic_scale, ions, geo, move_ions, sctol, scsteps, &
                  	      maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, maxwell_sctol


# Initializing the Maxwell propagator
# File: td/maxwell_propagator.F90

	subroutine maxwell_propagator_init(maxwell_gr, maxwell_st, maxwell_hm, maxwell_tr)


# Applying the Maxwell propagator
# File: td/maxwell_propagator.F90

	subroutine maxwell_propagation_etrs(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, rs_state,   &
	                                      rs_current_density_t1, rs_current_density_t2,             & 
	                                      rs_charge_density_t1, rs_charge_density_t2, time, dt,     &
	                                      rs_state_pml_predict)


# Maxwell to matter grid mapping and matter to Maxwell grid mapping
# File: td/maxwell_propagator.F90

	subroutine maxwell_matter_mesh_mapping(gr, maxwell_gr)


# Applying the Maxwell exponential operator
# File: td/maxwell_propagator.F90

	subroutine maxwell_exponential_apply(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, time, dt, ff)


# Transforming a 3x3 RS vector into a 6x6 or 4x4 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_forward(maxwell_hm, maxwell_gr, maxwell_st, rs_state, ff_rs_state)


# Transforming a 6x6 or 4x4 RS vector into a 3x3 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_backward(maxwell_hm, maxwell_gr, maxwell_st, ff_rs_state, rs_state)


# Transforming a charge and current density into a 3x3 or 4x4 RS density
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_forward(maxwell_hm, rs_charge_density, rs_current_density, ff_density)


# Transforming a 3x3 or 4x4 RS density into charge and current density
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_backward(maxwell_hm, ff_density, rs_charge_density, rs_current_density)


# Transforming a 3x3 RS vector into a 6x6 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_to_6x6_rs_state_forward(rs_state_3x3_plus, rs_state_3x3_minus, rs_state_6x6)


# Transforming a 6x6 RS vector into a 3x3 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_to_6x6_rs_state_backward(rs_state_6x6, rs_state)


# Transforming a 3x3 RS current density vector into a 6x6 RS current density vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_to_6x6_rs_densities_forward(rs_charge_density, rs_current_density, rs_density_6x6)


# Transforming a 6x6 RS current density vector into a 3x3 RS current density vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_to_6x6_rs_densities_backward(rs_density_6x6, rs_charge_density, rs_current_density)


# Transforming a 3x3 RS vector into a 4x4 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_to_4x4_rs_state_forward(rs_state_3x3, rs_state_4x4)


# Transforming a 4x4 RS vector into a 3x3 RS vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_state_to_4x4_rs_state_backward(rs_state_4x4, rs_state_3x3)


# Transform RS charge and current density into a 4x4 RS density
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_to_4x4_rs_densities_forward(rs_charge_density, rs_current_density, rs_density_4x4)


# Transform a 4x4 RS density into RS charge and current density
# File: td/maxwell_propagator.F90

	subroutine maxwell_transform_rs_densities_to_4x4_rs_densities_backward(rs_density_4x4, rs_charge_density, rs_current_density)



# Calculation of the longitudinal electric field of the matter
# File: td/maxwell_propagator.F90

	subroutine maxwell_calculate_matter_longitudinal_field(maxwell_gr, maxwell_st, maxwell_hm, gr, st, hm, &
        	                                               maxwell_rs_state_matter, geo)


# Calculation of the transverse, longitudinal field and vector potential
# File: td/maxwell_propagator.F90

 	subroutine maxwell_get_vector_pot_and_transverse_field(trans_calc_method, maxwell_gr, maxwell_hm, maxwell_st, maxwell_tr, gr, &
        	                                               hm, st, tr, maxwell_poisson_solver, time, field, transverse_field, & 
                	                                       vector_potential, geo)


# Mapping of the inner and outer (boundary) Maxwell points
# File: td/maxwell_propagator.F90

	subroutine maxwell_inner_and_outer_points_mapping(maxwell_mesh, maxwell_st, bounds)


# Mapping of nine (3x3) grid points on each of the Maxwell grid surfaces
# File: td/maxwell_propagator.F90

	subroutine maxwell_surface_grid_points_mapping(maxwell_mesh, maxwell_st, bounds)


# Calculating the Maxwell energy density
# File: td/maxwell_propagator.F90

	subroutine maxwell_energy_density_calc(maxwell_gr, maxwell_st, rs_field, maxwell_energy_dens, &
	                                       maxwell_e_energy_dens, maxwell_b_energy_dens, plane_waves_check, &
	                                       rs_field_plane_waves, maxwell_energy_dens_plane_waves)

# Calculating the Maxwell energy
# File: td/maxwell_propagator.F90

	subroutine maxwell_energy_calc(maxwell_gr, maxwell_st, maxwell_hm, rs_field, mx_energy, mx_e_energy, mx_b_energy, &
	                               mx_energy_boundary, rs_field_plane_waves, mx_energy_plane_waves)


# Applying the proper mask absorbing boundaries
# File: td/maxwell_propagator.F90

	subroutine maxwell_mask_absorbing_boundaries(maxwell_gr, maxwell_hm, maxwell_st, maxwell_tr, time, dt, time_delay, rs_state)


# Maxwell mask function routine
# File: td/maxwell_propagator.F90

	subroutine maxwell_mask(maxwell_gr, maxwell_hm, maxwell_st, rs_state)


# First perfectly matched layer stage
# File: td/maxwell_propagator.F90

	subroutine maxwell_pml_propagation_stage_1(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, ff_rs_state, ff_rs_state_pml)


# Second perfectly matched layer state
# File: td/maxwell_propagator.F90

	subroutine maxwell_pml_propagation_stage_2(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, time, dt, time_delay, &
	                                           ff_rs_state_pml, ff_rs_state)


# Perfectly matched layer convolution function update
# File: td/maxwell_propagator.F90

	subroutine maxwell_cpml_conv_function_update(maxwell_hm, maxwell_gr, ff_rs_state_pml, ff_dim)


# Perfectly matched layer convolution function update via Riemann-Silberstein vector
# File: td/maxwell_propagator.F90

	subroutine maxwell_cpml_conv_function_update_via_riemann_silberstein(maxwell_hm, maxwell_gr, ff_rs_state_pml, ff_dim)


# Perfectly matched layer convolution function update directly via electric and magnetic field
# File: td/maxwell_propagator.F90

	subroutine maxwell_cpml_conv_function_update_via_e_b_fields(maxwell_hm, maxwell_gr, ff_rs_state_pml, ff_dim)


# Generating medium boxes 
# File: td/maxwell_propagator.F90

	subroutine maxwell_generate_medium_boxes(maxwell_hm, maxwell_gr, nr_of_boxes)


# get Maxwell to matter coupling points (center of mass / center of charge / ions)
# File: td/maxwell_propagator.F90

	subroutine maxwell_get_mx_ma_coupling_points(geo, hm, mx_ma_coupling_points)


# Mapping of all grid points to a certain coupling point (can be removed probably)
# File: td/maxwell_propagator.F90

	subroutine maxwell_grid_points_coupling_points_mapping(gr, mx_ma_coupling_points, mx_ma_coupling_points_number, &
	                                                       mx_ma_coupling_map)


# TD function initialization for spatial constant Maxwell field calculation
# File: td/maxwell_propagator.F90

	subroutine maxwell_td_function_init(maxwell_st, maxwell_gr, maxwell_hm)


# Spatial constant Maxwell field calculation (spatial constant delta value is added to the Riemann-Silberstein vector)
# File: td/maxwell_propagator.F90

	subroutine maxwell_spatial_constant_calculation(constant_calc, maxwell_st, maxwell_gr, maxwell_hm, time, dt, delay, rs_state)


# Setting a certain analytical time evolved calculated value for all boundary points
# File: td/maxwell_propagator.F90

	subroutine maxwell_constant_boundaries_calculation(constant_calc, maxwell_bc, maxwell_hm, maxwell_st, rs_state)


# Setting an analytical calculated plane waves value for all boundary points
# File: td/maxwell_propagator.F90

	subroutine maxwell_plane_waves_boundaries_calculation(maxwell_hm, maxwell_tr, maxwell_st, maxwell_mesh, time, time_delay, rs_state)


# Plane wave propagation inside the free propagation region
# File: td/maxwell_propagation.90

	subroutine maxwell_plane_waves_propagation(maxwell_hm, maxwell_st, maxwell_gr, maxwell_tr, time, dt, time_delay)


# Analytical plane wave calculation inside the whole simulation box
# File: td/maxwell_propagation.F90

	subroutine maxwell_plane_waves_in_box_calculation(maxwell_bc, time, maxwell_gr, maxwell_st, maxwell_hm, rs_state)






###################################################################################################################################
# Write Maxwell data routines 
#


# Initialization of writing td Maxwell files
# td/td_write.F90

	subroutine maxwell_td_write_init(writ, maxwell_gr, maxwell_st, maxwell_hm, iter, max_iter, dt)


# Writing Maxwell values for current time step
# td/td_write.F90

	subroutine maxwell_td_write_iter(writ, maxwell_gr, maxwell_st, maxwell_hm, dt, iter, hm, geo)


# Writing Maxwell energy for current time step
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_energy(out_maxwell_energy, maxwell_hm, maxwell_st, iter, hm, ke)


# Writing Maxwell energy for calculating the absorption spectrum directly via Maxwell fields
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_spectrum_energy(out_maxwell_spectrum_energy, maxwell_hm, maxwell_st, iter)


# Writing Maxwell poynting vector at the simulation box surface
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_poynting_vector_box_surface(out_maxwell_poynting_surf, maxwell_hm, maxwell_st, dim, iter)


# Writing the electric field at the simulation box surface
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_electric_field_box_surface(out_maxwell_field_surf, maxwell_hm, maxwell_st, dim, iter)


# Writing the magnetic field at the simulation box surface
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_magnetic_field_box_surface(out_maxwell_field_surf, maxwell_hm, maxwell_st, dim, iter)


# Writing the mean poynting vector
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_poynting_vector(out_maxwell_poynting, maxwell_st, maxwell_gr, iter, dt, plane_wave_flag)


# Writing the Maxwell fields for certain coordinate
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_fields(out_maxwell_fields, maxwell_st, maxwell_gr, iter, dt)


# Writing the Maxwell datas 
# td/td_write.F90

	subroutine maxwell_td_write_maxwell_data(writ, gr, maxwell_gr, st, maxwell_st, hm, maxwell_hm, geo, maxwell_outp, iter, dt)






###################################################################################################################################
# Maxwell Hamiltonian routines 
#


# Initializing the Maxwell Hamiltonian
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_hamiltonian_init(maxwell_hm, maxwell_gr, maxwell_st, maxwell_geo)


# Maxwell Hamiltonian update (here only the time is updated, can maybe be added to another routine)
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_hamiltonian_update(this, mesh, st, time)


# Applying the Maxwell Hamiltonian on Maxwell states
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_hamiltonian_apply(maxwell_hm, maxwell_der, psi, oppsi, time)


# Applying the Maxwell Hamiltonian on Maxwell states with finite difference
# hamiltonian/maxwell_hamiltonian.F90

subroutine maxwell_hamiltonian_apply_fd(maxwell_hm, maxwell_der, psi, oppsi)


# Maxwell Hamiltonian is updated for the PML calculation
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_pml_hamiltonian(maxwell_hm, maxwell_der, psi, dir1, dir2, tmp)


# Maxwell Hamiltonian is updated for the PML calculation via Riemann-Silberstein vector
# hamiltonian/maxwell_hamiltonian.F90

subroutine maxwell_pml_calculation_via_riemann_silberstein(maxwell_hm, maxwell_der, psi, pml_dir, field_dir, pml)


# Maxwell Hamiltonian is updated for the PML calculation directly via electric and magnetic field
# hamiltonian/maxwell_hamiltonian.F90

subroutine maxwell_pml_calculation_via_e_b_fields(maxwell_hm, maxwell_der, psi, pml_dir, field_dir, pml)


# Maxwell Hamiltonian is updated for the PML calculation via Riemann-Silberstein vector with medium inside the box
# hamiltonian/maxwell_hamiltonian.F90

subroutine maxwell_pml_calculation_via_riemann_silberstein_medium(maxwell_hm, maxwell_der, psi, pml_dir, field_dir, pml)


# Maxwell Hamiltonian is updated for the PML calculation directly via electric and magnetic field with medium inside the box
# hamiltonian/maxwell_hamiltonian.F90

subroutine maxwell_pml_calculation_via_e_b_fields_medium(maxwell_hm, maxwell_der, psi, pml_dir, field_dir, pml)


# Maxwell Hamiltonian for medium boundaries
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_medium_boundaries_calculation(maxwell_hm, maxwell_der, psi, oppsi)


# Maxwell Hamiltonian including medium boxes
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_medium_boxes_calculation(maxwell_hm, maxwell_der, psi, oppsi


# Maxwell Hamiltonian as FFT
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_fft_hamiltonian(maxwell_hm, k_vec, ff_dim, k_index_x, k_index_y, k_index_z, sigma, hm_matrix)


# Helmholtz decomposition to calculate a transverse field (maybe should be a general math function)
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_helmholtz_decomposition_trans_field(maxwell_poisson_solver, maxwell_gr, maxwell_hm, hm, maxwell_st, transverse_field)


# Helmholtz decomposition to calculate a longitudinal field (maybe should be a general math function)
# hamiltonian/maxwell_hamiltonian.F90

	subroutine maxwell_helmholtz_decomposition_long_field(maxwell_poisson_solver, maxwell_gr, longitudinal_field)


# Surface integral of the Helmholtz decomposition to calculate the transverse field (maybe should be a general math function)
# hamiltonian/maxwell_hamiltonian.F90

	subroutine surface_integral_helmholtz_transverse(maxwell_gr, maxwell_st, pos, field, surface_integral)
