#!/bin/bash


###################################################################################################
# figures folder names and definition of plot variables

# figures folders
main_figures_folder_scalar="density         density_difference  maxwell_energy_density"
main_figures_folder_field="maxwell_b_field  maxwell_e_field     maxwell_e_field_trans  maxwell_test_output_real maxwell_test_output_imag"

# double scalar figures folders
main_double_figures_folder_scalar_1="density            maxwell_energy_density"
main_double_figures_folder_scalar_2="density_difference maxwell_energy_density_difference"

# double field figures folders
main_double_figures_folder_field_1="maxwell_b_field            maxwell_e_field"
main_double_figures_folder_field_2="maxwell_b_field_difference maxwell_e_field_difference"

# quadruple scalar figures folders
main_quadruple_figures_folder_scalar_1="density"
main_quadruple_figures_folder_scalar_2="density_difference"
main_quadruple_figures_folder_scalar_3="maxwell_energy_density"
main_quadruple_figures_folder_scalar_4="maxwell_energy_density_difference"

# quadruple field figures folders
main_quadruple_figures_folder_field_1="maxwell_e_field"
main_quadruple_figures_folder_field_2="maxwell_e_field_difference"
main_quadruple_figures_folder_field_3="maxwell_b_field"
main_quadruple_figures_folder_field_4="maxwell_b_field_difference"

# quadruple field and scalar figures folders
main_quadruple_figures_folder_mixed_field_1="maxwell_b_field            maxwell_e_field"
main_quadruple_figures_folder_mixed_field_2="maxwell_e_field_difference maxwell_e_field_difference"
main_quadruple_figures_folder_mixed_scalar_3="density                   density"
main_quadruple_figures_folder_mixed_scalar_4="density_difference        density_difference"

# direction
plot_direction="x y z"
field_direction="x y z"

# plot limits
xlim1_scalar_list="-10.0    -10.0 -10.0"
xlim2_scalar_list=" 10.0     10.0  10.0"
ylim1_scalar_list="-10.0    -10.0 -10.0"
ylim2_scalar_list=" 10.0     10.0  10.0"
zlim1_scalar_list=" 0.0     -1e-5  0.0"
zlim2_scalar_list=" 0.015    1e-5  1.0"

xlim1_field_list="-10.0   -10.0   -10.0    -10.0  -10.0"
xlim2_field_list=" 10.0    10.0    10.0     10.0   10.0"
ylim1_field_list="-10.0   -10.0   -10.0    -10.0  -10.0"
ylim2_field_list=" 10.0    10.0    10.0     10.0   10.0"
zlim1_field_list="-0.008  -1.2e-1   -1e-0    -3e-1  -1e-1"
zlim2_field_list=" 0.008   1.2e-1    1e-0     3e-1   1e-1"

# title
title_scalar_array[0]="density"
title_scalar_array[1]="density difference"
title_scalar_array[2]="maxwell energy density"

title_field_array[0]="magnetic field"
title_field_array[1]="electric field"
title_field_array[2]="electric field difference"
title_field_array[3]="test function real"
title_field_array[4]="test function imag"

# xlabel and ylabel
xlabel_list="y z x"
ylabel_list="z x y"

# zlabel
zlabel_scalar_array[0]="electron density"
zlabel_scalar_array[1]="electron density difference"
zlabel_scalar_array[2]="maxwell energy density"

zlabal_field_array[0]="magnetic field"
zlabel_field_array[1]="electric field"
zlabel_field_array[2]="electric field difference"
zlabel_field_array[3]="test function real"
zlabel_field_array[4]="test function imag"

# palette
palette_scalar_array[0]="\$zlim1 \"gray90\", \$zlim2 \"forest-green\""
palette_scalar_array[1]="\$zlim1 \"brown\", 0.0 \"gray90\", \$zlim2 \"forest-green\""
palette_scalar_array[2]="\$zlim1 \"brwon\", 0.0 \"gray90\", \$zlim2 \"forest-green\""

palette_field_array[0]="\$zlim1 \"blue\", 0.0 \"gray90\", \$zlim2 \"red\""
palette_field_array[1]="\$zlim1 \"blue\", 0.0 \"gray90\", \$zlim2 \"red\""
palette_field_array[2]="\$zlim1 \"blue\", 0.0 \"gray90\", \$zlim2 \"red\""
palette_field_array[3]="\$zlim1 \"blue\", 0.0 \"gray90\", \$zlim2 \"red\""
palette_field_array[4]="\$zlim1 \"blue\", 0.0 \"gray90\", \$zlim2 \"red\""


###################################################################################################
# make sh scripts for scalar figures figures

folder_scalar_array=($main_figures_folder_scalar)

xlim1_array=($xlim1_scalar_list)
xlim2_array=($xlim2_scalar_list)
ylim1_array=($ylim1_scalar_list)
ylim2_array=($ylim2_scalar_list)
zlim1_array=($zlim1_scalar_list)
zlim2_array=($zlim2_scalar_list)

xlabel_array=($xlabel_scalar_list)
ylabel_array=($ylabel_scalar_list)

eof="EOF"

(( seq_limit=${#folder_scalar_array[@]}-1 ))

for ii in `seq 0 $seq_limit`
do
  mkdir ${folder_scalar_array[$ii]}
  for jj in $plot_direction
  do
    figures_folder="./"${folder_scalar_array[$ii]}"/"${folder_scalar_array[$ii]}"."$jj"=0"
    mkdir $figures_folder
    cp ./run_dir_list ./$figures_folder/run_dir_list
    figures_script=$figures_folder"/script_"${folder_scalar_array[$ii]}"_figures."$jj"=0.sh"
    movie_script=$figures_folder"/script_"${folder_scalar_array[$ii]}"_movie."$jj"=0.sh"
    data_file=${folder_scalar_array[$ii]}"."$jj"=0"

    cat <<- EOF > $figures_script
#!/bin/bash

# data files which should be plot
data_file="$data_file"

# plot limits
xlim1=${xlim1_array[$ii]}
xlim2=${xlim2_array[$ii]}
ylim1=${ylim1_array[$ii]}
ylim2=${ylim2_array[$ii]}
zlim1=${zlim1_array[$ii]}
zlim2=${zlim2_array[$ii]}

# title
title="${title_scalar_array[$ii]}"

# label names 
xlabel="${xlabel_scalar_array[$ii]} [a.u.]"
ylabel="${ylabel_scalar_array[$ii]} [a.u.]"
#zlabel="${zlabel_scalar_array[$ii]} [a.u.]"

# fonts
font="Times-Roman"
titlefont=" \$font, 15"
xlabelfont="\$font, 15"
ylabelfont="\$font, 15"
zlabelfont="\$font, 15"
xticsfont=" \$font, 12"
yticsfont=" \$font, 12"
zticsfont=" \$font, 12"
cbticsfont="\$font, 10"

# image
pngsize=512,512
view=0,0
#view=72,345

zlim_half=\$( echo "(\$zlim1+\$zlim2)/2" | bc -l)

while read run_dir
do

  output_iter_dir=\$run_dir/output_iter/
  output_iter_dir_number=\$(echo \$output_iter_dir | awk -F/ '{print NF}')
  run_dir_number=\$(echo "\$output_iter_dir_number - 2" | bc)
  run_dir_solo=\$(echo \$output_iter_dir | cut -d'/' -f\$run_dir_number)
  echo \$run_dir_solo

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  td_folder_list=\$run_dir/output_iter/td.*

  for ii in \$td_folder_list
  do
    folder_name=\$( echo \$ii | cut -d'/' -f\$output_iter_dir_number)
    data_dir_and_name=./figures/\$run_dir_solo/\$folder_name/\$data_file

    if [ -d ./figures/\$run_dir_solo/\$folder_name ] ; then
      echo ./figures/\$run_dir_solo/\$folder_name
    else
      mkdir ./figures/\$run_dir_solo/\$folder_name
      echo ./figures/\$run_dir_solo/\$folder_name
    fi

    cp \$ii/\$data_file ./figures/\$run_dir_solo/\$folder_name

    cat <<- $eof > ./figures/\$run_dir_solo/\$folder_name/gnuplot_contour3D.sh
#!/usr/bin/gnuplot

  set xlabel "\$xlabel"
#  set xlabel font "\$xlabelfont"
  set xlabel "x-direction" offset 0,1.1
  set ylabel "\$ylabel"
#  set ylabel font "\$ylabelfont"
  set ylabel "y-direction" offset -33.0,0 rotate parallel
#  set zlabel "\$zlabel" rotate by 90 left
#  set zlabel font "\$zlabelfont"
#  set zlabel offset 1,0
#  set xtics font "\$xticsfont"
  set xtics offset 0,0.0 out (-10,-5,0,5,10)
#  set ytics font "\$yticsfont"
  set ytics offset -30.0,0 out (-10,-5,0,5,10)
#  set ztics font "\$zticsfont"
  set ztics format ""
#  set cbtics font "\$cbticsfont"
  set pm3d at bs
  set output "\$data_file.png"
  set terminal png size \$pngsize crop
  set view \$view
  set palette defined (${palette_scalar_array[$ii]})
  set cbrange [\$zlim1:\$zlim2]
#  set title "\$title" font "\$titlefont"
  set view equal xyz
  splot [\$xlim1:\$xlim2] [\$ylim1:\$ylim2] [\$zlim1:\$zlim2] "\$data_file" with pm3d t ""
$eof

    pushd ./figures/\$run_dir_solo/\$folder_name/
    chmod 777 gnuplot_contour3D.sh
    ./gnuplot_contour3D.sh
    popd

    cp ./figures/\$run_dir_solo/\$folder_name/\$data_file.png ./figures/\$run_dir_solo/figures_serial/\$folder_name.\$data_file.png

 # rm ./\$data_dir_and_name
  echo ""

  done

done < "run_dir_list"

EOF

    cat <<- EOF > $movie_script
#!/bin/bash

data_file="$ii.$jj=0"

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)
  mkdir ./movie
  mkdir ./movie/\$run_dir_solo

  movie_png_list=""

  count=0
  for ii in figures/\$run_dir_solo/figures_serial/td.*.\$data_file.png
  do
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    cp \$ii ./movie/\$run_dir_solo/td.\$jj.\$data_file.png
    movie_png_list="\$movie_png_list \$ii"
    count=\$(echo "\$count+1" | bc)
  done

  convert -delay 2  \$movie_png_list  ./movie/\$run_dir_solo/\$data_file.z=0.mov

done < "run_dir_list"

EOF

    chmod 777 $figures_script
    chmod 777 $movie_script
  done  
done


###################################################################################################
# make sh scripts for field figures figures

folder_field_array=($main_figures_folder_field)

xlim1_array=($xlim1_field_list)
xlim2_array=($xlim2_field_list)
ylim1_array=($ylim1_field_list)
ylim2_array=($ylim2_field_list)
zlim1_array=($zlim1_field_list)
zlim2_array=($zlim2_field_list)

xlabel_array=($xlabel_field_list)
ylabel_array=($ylabel_field_list)

(( seq_limit=${#folder_field_array[@]}-1 ))

for ii in `seq 0 $seq_limit`
do
  mkdir ${folder_field_array[$ii]}
  for jj in $plot_direction
  do
    for kk in $field_direction
    do
      if   [ $kk == 1 ]
      then
        kstring="x"
      elif [ $kk == 2 ]
      then
        kstring="y"
      elif [ $kk == 3 ]
      then
        kstring="z"
      fi
      figures_folder="./"${folder_field_array[$ii]}"/"${folder_field_array[$ii]}"-"$kk"."$jj"=0"
      mkdir $figures_folder
      cp ./run_dir_list ./$figures_folder/run_dir_list
      figures_script=$figures_folder"/script_"${folder_field_array[$ii]}"-"$kk"_figures."$jj"=0.sh"
      movie_script=$figures_folder"/script_"${folder_field_array[$ii]}"-"$kk"_movie."$jj"=0.sh"
      data_file=${folder_field_array[$ii]}"-"$kk"."$jj"=0"

      cat <<- EOF > $figures_script
#!/bin/bash

# data files which should be plot
data_file="$data_file"

# plot limits
xlim1=${xlim1_array[$ii]}
xlim2=${xlim2_array[$ii]}
ylim1=${ylim1_array[$ii]}
ylim2=${ylim2_array[$ii]}
zlim1=${zlim1_array[$ii]}
zlim2=${zlim2_array[$ii]}

# title
title="${title_field_array[$ii]} in $kstring direction"

# label names 
xlabel="${xlabel_field_array[$ii]} [a.u.]"
ylabel="${ylabel_field_array[$ii]} [a.u.]"
zlabel="${zlabel_field_array[$ii]} [a.u.]"

# fonts
font="Times-Roman"
titlefont=" \$font, 15"
xlabelfont="\$font, 15"
ylabelfont="\$font, 15"
zlabelfont="\$font, 15"
xticsfont=" \$font, 12"
yticsfont=" \$font, 12"
zticsfont=" \$font, 12"
cbticsfont="\$font, 10"

# image
pngsize=512,512
view=0,0
#view=72,345

zlim_half=\$( echo "(\$zlim1+\$zlim2)/2" | bc -l)

while read run_dir
do

  output_iter_dir=\$run_dir/output_iter/
  output_iter_dir_number=\$(echo \$output_iter_dir | awk -F/ '{print NF}')
  run_dir_number=\$(echo "\$output_iter_dir_number - 2" | bc)
  run_dir_solo=\$(echo \$output_iter_dir | cut -d'/' -f\$run_dir_number)
  echo \$run_dir_solo

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  td_folder_list=\$run_dir/output_iter/td.*

  for ii in \$td_folder_list
  do
    folder_name=\$( echo \$ii | cut -d'/' -f\$output_iter_dir_number)
    data_dir_and_name=./figures/\$run_dir_solo/\$folder_name/\$data_file

    if [ -d ./figures/\$run_dir_solo/\$folder_name ] ; then
      echo ./figures/\$run_dir_solo/\$folder_name
    else
      mkdir ./figures/\$run_dir_solo/\$folder_name
      echo ./figures/\$run_dir_solo/\$folder_name
    fi

    cp \$ii/\$data_file ./figures/\$run_dir_solo/\$folder_name

    cat <<- $eof > ./figures/\$run_dir_solo/\$folder_name/gnuplot_contour3D.sh
#!/usr/bin/gnuplot

  set xlabel "\$xlabel"
#  set xlabel font "\$xlabelfont"
  set xlabel "x-direction" offset 0,1.1
  set ylabel "\$ylabel"
#  set ylabel font "\$ylabelfont"
  set ylabel "y-direction" offset -33.0,0 rotate parallel
#  set zlabel "\$zlabel" rotate by 90 left
#  set zlabel font "\$zlabelfont"
#  set zlabel offset 1,0
#  set xtics font "\$xticsfont"
  set xtics offset 0,0.0 out (-10,-5,0,5,10)
#  set ytics font "\$yticsfont"
  set ytics offset -30.0,0 out (-10,-5,0,5,10)
#  set ztics font "\$zticsfont"
  set ztics format ""
#  set cbtics font "\$cbticsfont"
  set pm3d at bs
  set output "\$data_file.png"
  set terminal png size \$pngsize crop
  set view \$view
  set palette defined (${palette_field_array[$ii]})
  set cbrange [\$zlim1:\$zlim2]
#  set title "\$title" font "\$titlefont"
  set view equal xyz
  splot [\$xlim1:\$xlim2] [\$ylim1:\$ylim2] [\$zlim1:\$zlim2] "\$data_file" with pm3d t ""
$eof

    pushd ./figures/\$run_dir_solo/\$folder_name/
    chmod 777 gnuplot_contour3D.sh
    ./gnuplot_contour3D.sh
    popd

    cp ./figures/\$run_dir_solo/\$folder_name/\$data_file.png ./figures/\$run_dir_solo/figures_serial/\$folder_name.\$data_file.png

 # rm ./\$data_dir_and_name
  echo ""

  done

done < "run_dir_list"

EOF

    cat <<- EOF > $movie_script
#!/bin/bash

data_file="$data_file"

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)
  mkdir ./movie
  mkdir ./movie/\$run_dir_solo

  movie_png_list=""

  count=0
  for ii in figures/\$run_dir_solo/figures_serial/td.*.\$data_file.png
  do
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    cp \$ii ./movie/\$run_dir_solo/td.\$jj.\$data_file.png
    movie_png_list="\$movie_png_list \$ii"
    count=\$(echo "\$count+1" | bc)
  done

  convert -delay 2  \$movie_png_list  ./movie/\$run_dir_solo/\$data_file.z=0.mov

done < "run_dir_list"

EOF

      chmod 777 $figures_script
      chmod 777 $movie_script
    done
  done
done


###################################################################################################
# scripts for multi figures
mkdir multi_figures

###################################################################################################
# make sh scripts for scalar double plot figures and make sh script for corresponding movies

folder_scalar_1_array=($main_double_figures_folder_scalar_1)
folder_scalar_2_array=($main_double_figures_folder_scalar_2)

(( seq_limit= $( echo ${#folder_scalar_1_array[@]}) - 1 ))
for ii in `seq 0 $seq_limit`
do
  figures_folder_1="${folder_scalar_1_array[$ii]}"
  figures_folder_2="${folder_scalar_2_array[$ii]}"
  figures_folder=$figures_folder_1"_plus_"$figures_folder_2
  mkdir ./multi_figures/$figures_folder
  for jj in $plot_direction
  do
    figures_folder_dim=$figures_folder_1"_plus_"$figures_folder_2"."$jj"=0"
    mkdir ./multi_figures/$figures_folder/$figures_folder_dim
    cp ./run_dir_list ./multi_figures/$figures_folder/$figures_folder_dim/run_dir_list
    figures_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder"_figures."$jj"=0.sh"
    movie_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder"_movie."$jj"=0.sh"

    cat <<- EOF > $figures_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  figures_list="../../../$figures_folder_1/${figures_folder_1}.$jj=0/figures/\$run_dir_solo/figures_serial/td.*"

  count=0
  for ii in \$figures_list
  do 
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    figure_1="../../../$figures_folder_1/${figures_folder_1}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_1.$jj=0.png"
    figure_2="../../../$figures_folder_2/${figures_folder_2}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_2.$jj=0.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder}.$jj=0.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    count=\$( echo "\$count+1" | bc)
  done
done < "run_dir_list"

EOF

    cat <<- EOF > $movie_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./movie
  mkdir ./movie/run_dir_solo
  mkdir ./movie/run_dir_solo/figures_serial

  count=0
  movie_png_list=""
  for ii in ./figures/rund_dir_solo/figures_serial/td.*.$figures_folder_dim.png
  do
    movie_png_list="\$movie_png_list  \$ii"
  done

  convert -delay 2  \$movie_png_list  ./movie/run_dir_solo/$figures_folder_dim.mov
done < "run_dir_list"

EOF

    chmod 777 $figures_script
    chmod 777 $movie_script
  done
done


###################################################################################################
# make sh scripts for field double plot figures and make sh script for corresponding movies

folder_field_1_array=($main_double_figures_folder_field_1)
folder_field_2_array=($main_double_figures_folder_field_2)

(( seq_limit= $( echo ${#folder_field_1_array[@]}) - 1 ))
for ii in `seq 0 $seq_limit`
do
  figures_folder_1="${folder_field_1_array[$ii]}"
  figures_folder_2="${folder_field_2_array[$ii]}"
  figures_folder=$figures_folder_1"_plus_"$figures_folder_2
  mkdir ./multi_figures/$figures_folder
  for jj in $plot_direction
  do
    for kk in $field_direction
    do
      figures_folder_dim=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk"_."$jj"=0"
      figures_folder_dir=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk
      mkdir ./multi_figures/$figures_folder/$figures_folder_dim
      cp ./run_dir_list ./multi_figures/$figures_folder/$figures_folder_dim/run_dir_list
      figures_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_figures."$jj"=0.sh"
      movie_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_movie."$jj"=0.sh"

      cat <<- EOF > $figures_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial
  figure_list="../../../$figures_folder_1/${figures_folder_1}_$kk.$jj=0/figures/\$run_dir_solo/figures_serial/td.*"

  count=0
  for ii in \$figures_list
  do 
    jj="\$(printf '%07d' "\$count")"
    figure_1="../../../$figures_folder_1/${figures_folder_1}_$kk.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_1}_$kk.$jj=0.png"
    figure_2="../../../$figures_folder_2/${figures_folder_2}_$kk.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_2}_$kk.$jj=0.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_1}_$kk_plus_${figures_folder_1}_$kk.$jj=0.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    count=\$( echo "\$count+1" | bc)
  done
done < "run_dir_list"

EOF

      cat <<- EOF > $movie_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./movie
  mkdir ./movie/run_dir_solo
  mkdir ./movie/run_dir_solo/figures_serial

  count=0
  movie_png_list=""
  for ii in ./figures/rund_dir_solo/figures_serial/td.*.$figures_folder_dim.png
  do
    movie_png_list="\$movie_png_list  \$ii"
  done

  convert -delay 2  \$movie_png_list  ./movie/run_dir_solo/$figures_folder_dim.mov
done < "run_dir_list"

EOF

      chmod 777 $figures_script
      chmod 777 $movie_script
    done
  done
done


###################################################################################################
# make sh scripts for scalar quadruple plot figures and make sh script for corresponding movies

folder_scalar_1_array=($main_quadruple_figures_folder_scalar_1)
folder_scalar_2_array=($main_quadruple_figures_folder_scalar_2)
folder_scalar_3_array=($main_quadruple_figures_folder_scalar_3)
folder_scalar_4_array=($main_quadruple_figures_folder_scalar_4)

(( seq_limit= $( echo ${#folder_scalar_1_array[@]}) - 1 ))
for ii in `seq 0 $seq_limit`
do
  figures_folder_1="${folder_scalar_1_array[$ii]}"
  figures_folder_2="${folder_scalar_2_array[$ii]}"
  figures_folder_3="${folder_scalar_3_array[$ii]}"
  figures_folder_4="${folder_scalar_4_array[$ii]}"
  figures_folder=$figures_folder_1"_plus_"$figures_folder_2"_and_"$figures_folder_3"_plus_"$figures_folder_4
  mkdir ./multi_figures/$figures_folder
  for jj in $plot_direction
  do
    figures_folder_1_dim=${figures_folder_1}.$jj=0
    figures_folder_2_dim=${figures_folder_2}.$jj=0
    figures_folder_3_dim=${figures_folder_3}.$jj=0
    figures_folder_4_dim=${figures_folder_4}.$jj=0
    figures_folder_dim=$figures_folder_1"_plus_"$figures_folder_2"_and_"$figures_folder_3"_plus_"$figures_folder_4"."$jj"=0"
    mkdir ./multi_figures/$figures_folder/$figures_folder_dim
    cp ./run_dir_list ./multi_figures/$figures_folder/$figures_folder_dim/run_dir_list
    figures_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder"_figures."$jj"=0.sh"
    movie_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder"_movie."$jj"=0.sh"

    cat <<- EOF > $figures_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  figures_list="../../../$figures_folder_1/${figures_folder_1}.$jj=0/figures/\$run_dir_solo/figures_serial/td.*"

  count=0
  for ii in \$figures_list
  do 
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    figure_1="../../../$figures_folder_1/${figures_folder_1}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_1.$jj=0.png"
    figure_2="../../../$figures_folder_2/${figures_folder_2}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_2.$jj=0.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_1="../../../$figures_folder_3/${figures_folder_3}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_3.$jj=0.png"
    figure_2="../../../$figures_folder_4/${figures_folder_4}.$jj=0/figures/\$run_dir_solo/figures_serial/td.\$jj.$figures_folder_4.$jj=0.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_dim}.png"
    convert ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png -append \$figure_combi
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png
    count=\$( echo "\$count+1" | bc)
  done
done < "run_dir_list"

EOF

    cat <<- EOF > $movie_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./movie
  mkdir ./movie/\$run_dir_solo
  mkdir ./movie/\$run_dir_solo/figures_serial

  count=0
  movie_png_list=""
  for ii in ./figures/rund_dir_solo/figures_serial/td.*.$figures_folder_dim.png
  do
    movie_png_list="\$movie_png_list  \$ii"
  done

  convert -delay 2  \$movie_png_list  ./movie/run_dir_solo/$figures_folder_dim.mov
done < "run_dir_list"

EOF

    chmod 777 $figures_script
    chmod 777 $movie_script
  done
done


###################################################################################################
# make sh scripts for field quadruple plot figures and make sh script for corresponding movies

folder_field_1_array=($main_quadruple_figures_folder_field_1)
folder_field_2_array=($main_quadruple_figures_folder_field_2)
folder_field_3_array=($main_quadruple_figures_folder_field_3)
folder_field_4_array=($main_quadruple_figures_folder_field_4)

(( seq_limit= $( echo ${#folder_field_1_array[@]}) - 1 ))
for ii in `seq 0 $seq_limit`
do
  figures_folder_1="${folder_field_1_array[$ii]}"
  figures_folder_2="${folder_field_2_array[$ii]}"
  figures_folder_3="${folder_field_3_array[$ii]}"
  figures_folder_4="${folder_field_4_array[$ii]}"
  figures_folder=$figures_folder_1"_plus_"$figures_folder_2"_and_"$figures_folder_3"_plus_"$figures_folder_4
  mkdir ./multi_figures/$figures_folder
  for jj in $plot_direction
  do
    for kk in $field_direction
    do
      figures_folder_1_dir=${figures_folder_1}_$kk
      figures_folder_2_dir=${figures_folder_2}_$kk
      figures_folder_3_dir=${figures_folder_3}_$kk
      figures_folder_4_dir=${figures_folder_4}_$kk
      figures_folder_1_dim=${figures_folder_1}_$kk.$jj=0
      figures_folder_2_dim=${figures_folder_2}_$kk.$jj=0
      figures_folder_3_dim=${figures_folder_3}_$kk.$jj=0
      figures_folder_4_dim=${figures_folder_4}_$kk.$jj=0
      figures_folder_dir=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk"_and_"$figures_folder_3"_"$kk"_plus_"$figures_folder_4"_"$kk
      figures_folder_dim=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk"_and_"$figures_folder_3"_"$kk"_plus_"$figures_folder_4"_"$kk"."$jj"=0"
      mkdir ./multi_figures/$figures_folder/$figures_folder_dim
      cp ./run_dir_list ./multi_figures/$figures_folder/$figures_folder_dim/run_dir_list
      figures_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_figures."$jj"=0.sh"
      movie_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_movie."$jj"=0.sh"

      cat <<- EOF > $figures_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  figures_list="../../../$figures_folder_1/${figures_folder_1_dim}/figures/\$run_dir_solo/figures_serial/td.*"

  count=0
  for ii in \$figures_list
  do 
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    figure_1="../../../$figures_folder_1/${figures_folder_1_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_1_dim}.png"
    figure_2="../../../$figures_folder_2/${figures_folder_2_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_2_dim}.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_1="../../../$figures_folder_3/${figures_folder_3_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_3_dim}.png"
    figure_2="../../../$figures_folder_4/${figures_folder_4_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_4_dim}.png"
    figures_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_dim}.png"
    convert ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png -append \$figure_combi
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png
    count=\$( echo "\$count+1" | bc)
  done
done < "run_dir_list"

EOF

      cat <<- EOF > $movie_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./movie
  mkdir ./movie/\$run_dir_solo
  mkdir ./movie/\$run_dir_solo/figures_serial

  count=0
  movie_png_list=""
  for ii in ./figures/rund_dir_solo/figures_serial/td.*.$figures_folder_dim.png
  do
    movie_png_list="\$movie_png_list  \$ii"
  done

  convert -delay 2  \$movie_png_list  ./movie/run_dir_solo/$figures_folder_dim.mov
done < "run_dir_list"

EOF

      chmod 777 $figures_script
      chmod 777 $movie_script
    done
  done
done


###################################################################################################
# make sh scripts for field quadruple plot figures and make sh script for corresponding movies

folder_field_1_array=($main_quadruple_figures_folder_mixed_field_1)
folder_field_2_array=($main_quadruple_figures_folder_mixed_field_2)
folder_field_3_array=($main_quadruple_figures_folder_mixed_scalar_3)
folder_field_4_array=($main_quadruple_figures_folder_mixed_scalar_4)

(( seq_limit= $( echo ${#folder_field_1_array[@]}) - 1 ))
for ii in `seq 0 $seq_limit`
do
  figures_folder_1="${folder_field_1_array[$ii]}"
  figures_folder_2="${folder_field_2_array[$ii]}"
  figures_folder_3="${folder_field_3_array[$ii]}"
  figures_folder_4="${folder_field_4_array[$ii]}"
  figures_folder=$figures_folder_1"_plus_"$figures_folder_2"_and_"$figures_folder_3"_plus_"$figures_folder_4
  mkdir ./multi_figures/$figures_folder
  for jj in $plot_direction
  do
    for kk in $field_direction
    do
      figures_folder_1_dir=${figures_folder_1}_$kk
      figures_folder_2_dir=${figures_folder_2}_$kk

      figures_folder_1_dim=${figures_folder_1}_$kk.$jj=0
      figures_folder_2_dim=${figures_folder_2}_$kk.$jj=0

      figures_folder_3_dim=${figures_folder_3}.$jj=0
      figures_folder_4_dim=${figures_folder_4}.$jj=0

      figures_folder_dir=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk"_and_"$figures_folder_3"_plus_"$figures_folder_4
      figures_folder_dim=$figures_folder_1"_"$kk"_plus_"$figures_folder_2"_"$kk"_and_"$figures_folder_3"_plus_"$figures_folder_4"."$jj"=0"
      mkdir ./multi_figures/$figures_folder/$figures_folder_dim
      cp ./run_dir_list ./multi_figures/$figures_folder/$figures_folder_dim/run_dir_list
      figures_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_figures."$jj"=0.sh"
      movie_script=./multi_figures/$figures_folder/$figures_folder_dim"/script_"$figures_folder_dir"_movie."$jj"=0.sh"

      cat <<- EOF > $figures_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./figures
  mkdir ./figures/\$run_dir_solo
  mkdir ./figures/\$run_dir_solo/figures_serial

  figures_list="../../../$figures_folder_1/${figures_folder_1_dim}/figures/\$run_dir_solo/figures_serial/td.*"

  count=0
  for ii in \$figures_list
  do 
    run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
    td_number=\$( echo "\$run_dir_number_1-1" | bc)
    td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
    jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
    figure_1="../../../$figures_folder_1/${figures_folder_1_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_1_dim}.png"
    figure_2="../../../$figures_folder_2/${figures_folder_2_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_2_dim}.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_1="../../../$figures_folder_3/${figures_folder_3_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_3_dim}.png"
    figure_2="../../../$figures_folder_4/${figures_folder_4_dim}/figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_4_dim}.png"
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png"
    convert \$figure_1 \$figure_2 +append \$figure_combi
    figure_combi="./figures/\$run_dir_solo/figures_serial/td.\$jj.${figures_folder_dim}.png"
    convert ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png -append \$figure_combi
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_1.png
    rm ./figures/\$run_dir_solo/figures_serial/td.\$jj.tmp_2.png
    count=\$( echo "\$count+1" | bc)
  done
done < "run_dir_list"

EOF

      cat <<- EOF > $movie_script
#!/bin/bash

while read run_dir
do

  run_dir_number=\$(echo \$run_dir | awk -F/ '{print NF}')
  run_dir_solo=\$( echo \$run_dir | cut -d'/' -f \$run_dir_number)

  mkdir ./movie
  mkdir ./movie/\$run_dir_solo
  mkdir ./movie/\$run_dir_solo/figures_serial

  count=0
  movie_png_list=""
  for ii in ./figures/rund_dir_solo/figures_serial/td.*.$figures_folder_dim.png
  do
    movie_png_list="\$movie_png_list  \$ii"
  done

  convert -delay 2  \$movie_png_list  ./movie/run_dir_solo/$figures_folder_dim.mov
done < "run_dir_list"

EOF

      chmod 777 $figures_script
      chmod 777 $movie_script
    done
  done
done

