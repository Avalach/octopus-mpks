#!/bin/bash

aa=6.27
radius=10.6*$aa

rm geometry_z_dir.bohr.xyz

idx=0
for ii in $(seq -10 10)
do
  for jj in $(seq -10 10)
  do
    x=$( echo " -$aa / 2 + 3 * $aa * $ii " | bc -l )
    y=0.0
    z=$( echo " - sqrt(3) / 2 * $aa + sqrt(3) * $aa * $jj " | bc -l )
    rr=$( echo " sqrt($x^2+$y^3+$z^2) " | bc -l )
    if [[ $( echo " $rr <= $radius " | bc -l ) -eq 1 ]]
    then
      idx=$( echo "$idx+1" | bc)
      xx[$idx]=$( printf "% 011.6f" $x )
      yy[$idx]=$( printf "% 011.6f" $y )
      zz[$idx]=$( printf "% 011.6f" $z )
    fi
  done
done

for ii in $(seq -10 10)
do
  for jj in $( seq -10 10)
  do
    x=$( echo " $aa / 2 + 3 * $aa * $ii " | bc -l )
    y=0.0
    z=$( echo " - sqrt(3) / 2 * $aa + sqrt(3) * $aa * $jj " | bc -l )
    rr=$( echo " sqrt($x^2+$y^3+$z^2) " | bc -l )
    if [[ $( echo " $rr <= $radius " | bc -l ) -eq 1 ]]
    then
      idx=$( echo "$idx+1" | bc)
      xx[$idx]=$( printf "% 011.6f" $x )
      yy[$idx]=$( printf "% 011.6f" $y )
      zz[$idx]=$( printf "% 011.6f" $z )
    fi
  done
done

for ii in $(seq -10 10)
do
  for jj in $( seq -10 10)
  do
    x=$( echo " - $aa + 3 * $aa * $ii " | bc -l )
    y=0.0
    z=$( echo " sqrt(3) * $aa * $jj " | bc -l )
    rr=$( echo " sqrt($x^2+$y^3+$z^2) " | bc -l )
    if [[ $( echo " $rr <= $radius " | bc -l ) -eq 1 ]]
    then
      idx=$( echo "$idx+1" | bc)
      xx[$idx]=$( printf "% 011.6f" $x )
      yy[$idx]=$( printf "% 011.6f" $y )
      zz[$idx]=$( printf "% 011.6f" $z )
    fi
  done
done

for ii in $(seq -10 10)
do
  for jj in $( seq -10 10)
  do
    x=$( echo " $aa + 3 * $aa * $ii " | bc -l )
    y=0.0
    z=$( echo " sqrt(3) * $aa * $jj " | bc -l )
    rr=$( echo " sqrt($x^2+$y^3+$z^2) " | bc -l )
    if [[ $( echo " $rr <= $radius " | bc -l ) -eq 1 ]]
    then
      idx=$( echo "$idx+1" | bc)
      xx[$idx]=$( printf "% 011.6f" $x )
      yy[$idx]=$( printf "% 011.6f" $y )
      zz[$idx]=$( printf "% 011.6f" $z )
    fi
  done
done


echo $idx >> geometry_z_dir.bohr.xyz
echo "generated a " >> geometry_z_dir.bohr.xyz

for ii in $( seq 1 $idx)
do
  echo " Na     ${xx[$ii]}     ${yy[$ii]}     ${zz[$ii]}" >> geometry_z_dir.bohr.xyz
done


