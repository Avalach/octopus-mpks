#!/bin/bash

scale_factor=0.5291772106712
for ii in $( ls .)
do
  if [[ $ii = *bohr*.xyz ]]
  then
    geo_file_bohr=$ii
    geo_file_ang=$( echo $geo_file_bohr | cut -d'.' -f1)".angstrom."$( echo $geo_file_bohr | cut -d'.' -f3)
    if [[ ! -e $geo_file_ang ]]
    then
      line_idx=0
      while read geo_file_line
      do
        line_idx=$( echo " $line_idx + 1 " | bc)
        if [[ $line_idx -lt 2 ]]
        then
          echo '  '$geo_file_line >> $geo_file_ang
        elif [[ $line_idx -eq 2 ]]
        then
          echo '  '$( echo $geo_file_line | cut -d' ' -f1) a >> $geo_file_ang
        elif [[ $line_idx -gt 2 ]]
        then
          atom_name=$( echo $geo_file_line | cut -d ' ' -f 1)
          geo_x_bohr=$( echo $geo_file_line | cut -d ' ' -f 2)
          geo_x_ang=$(  echo " $geo_x_bohr * $scale_factor " | bc -l)
          geo_x_ang=$( printf "%08.6f" "$geo_x_ang" )
          geo_y_bohr=$( echo $geo_file_line | cut -d ' ' -f 3)
          geo_y_ang=$(  echo " $geo_y_bohr * $scale_factor " | bc -l)
          geo_y_ang=$( printf "%08.6f" "$geo_y_ang" )
          geo_z_bohr=$( echo $geo_file_line | cut -d ' ' -f 4)
          geo_z_ang=$(  echo " $geo_z_bohr * $scale_factor " | bc -l)
          geo_z_ang=$( printf "%08.6f" "$geo_z_ang" )
          if [[ $(echo " $geo_x_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_ang}'   '${geo_y_ang}'   '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_ang}'   '${geo_y_ang}'    '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_ang}'    '${geo_y_ang}'   '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_ang}'   '${geo_y_ang}'   '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_ang}'    '${geo_y_ang}'    '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_ang}'   '${geo_y_ang}'    '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_ang}'    '${geo_y_ang}'   '${geo_z_ang} >> $geo_file_ang
          elif [[ $(echo " $geo_x_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_ang >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_ang >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_ang}'    '${geo_y_ang}'    '${geo_z_ang} >> $geo_file_ang
          fi
        #  echo '      '${atom_name}${geo_x_ang}${geo_y_ang}${geo_z_ang} >> $geo_file_ang
        fi
      done < $geo_file_bohr
    fi
  fi
done
