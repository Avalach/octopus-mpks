#!/bin/bash

zzmax=0.0
zzmin=100.0
while read line
do 
  atom=$( echo $line | cut -d' ' -f1 )
  xx=$(   echo $line | cut -d' ' -f2 )
  yy=$(   echo $line | cut -d' ' -f3 )
  zz=$(   echo $line | cut -d' ' -f4 )
  if [[ $( echo " $zz < $zzmin " | bc -l) -eq 1 ]]
  then
    zzmin=$zz
  fi
  if [[ $( echo " $zz > $zzmin " | bc -l) -eq 1 ]]
  then
    zzmax=$zz
  fi
  shift=000.050240 
done < tmp_geometry

while read line
do 
  atom=$( echo $line | cut -d' ' -f1 )
  xx=$(   echo $line | cut -d' ' -f2 )
  yy=$(   echo $line | cut -d' ' -f3 )
  zz=$(   echo $line | cut -d' ' -f4 )
  zz=$(   echo " $zz + $shift " | bc -l )
  xx=$( printf "% 011.6f" $xx )
  yy=$( printf "% 011.6f" $yy )
  zz=$( printf "% 011.6f" $zz )
  echo " Na     $xx     $yy     $zz"   >> new_geometry

done < tmp_geometry


