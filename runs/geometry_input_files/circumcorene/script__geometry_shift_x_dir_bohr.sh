
rm geometry_x_dir.bohr.xyz.new
ln=0
min=10.0
max=0.0
while read line
do
  ln=$( echo " $ln+1 " | bc)
  if [ $ln -gt 2 ]
  then
    xvalue=$( echo $line | cut -d' ' -f2)
    if [ $( echo "$xvalue < $min" | bc -l) -eq 1 ]
    then
      min=$xvalue
    fi
    if [ $( echo "$xvalue > $max" | bc -l) -eq 1 ]
    then
      max=$xvalue
    fi
  else
    echo " " $line >> geometry_x_dir.bohr.xyz.new
  fi
done < geometry_x_dir.bohr.xyz

offset=$( echo "scale=6; $min + ($max - $min)/2" | bc -l)

ln=0
while read line
do
  ln=$( echo " $ln+1 " | bc) 
  if [ $ln -gt 2 ] 
  then
    aname=$( echo $line | cut -d' ' -f1)
    xvalue=$( echo $line | cut -d' ' -f2)
    xvalue=$( echo "scale=6; $xvalue - $offset" | bc)
    yvalue=$( echo $line | cut -d' ' -f3)
    zvalue=$( echo $line | cut -d' ' -f4)

    printf "%s %9.6f %s %9.6f %s %9.6f\n" "      $aname                 " $xvalue " " $yvalue " " $zvalue >> geometry_x_dir.bohr.xyz.new

    #echo "  " $aname "              " $xvalue "   " $yvalue "   " $zvalue    >> geometry_x_dir.bohr.xyz.new

  fi

done < geometry_x_dir.bohr.xyz

