#!/bin/bash

alpha=-0.15

rm geometry_x_dir.angstrom.new.xyz

line_idx=0
while read line
do
  line_idx=$( echo " $line_idx + 1 " | bc)
  if [[ $line_idx -gt 2 ]]
  then
    aname=$( echo $line | cut -d' ' -f1)
    xx=$( echo $line | cut -d' ' -f2)
    yy=$( echo $line | cut -d' ' -f3)
    zz=$( echo $line | cut -d' ' -f4)
    echo $line
    echo $xx $yy $zz
    xxnew=$( echo " scale=10; c($alpha) * $xx - s($alpha) * $zz " | bc -l )
    zznew=$( echo " scale=10; s($alpha) * $xx + c($alpha) * $zz " | bc -l )
    echo $aname $xxnew $yy $zznew >> geometry_x_dir.angstrom.new.xyz
  else
    echo $line >> geometry_x_dir.angstrom.new.xyz
  fi
done < geometry_x_dir.angstrom.xyz
