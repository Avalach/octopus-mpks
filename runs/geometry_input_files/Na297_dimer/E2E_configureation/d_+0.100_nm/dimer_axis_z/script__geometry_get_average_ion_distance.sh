#!/bin/bash

#x=3.419490
#y=5.532840
#z=16.647699

#x=18.330400
#y=7.001590
#z=14.271200

x=9.109850
y=9.109850
z=34.709801

x=20.419100
y=10.474800
z=19.984600

min=100.0

idx=0
while read line
do
  idx=$( echo " $idx + 1 " | bc)
  if [[ $idx -gt 2 ]]
  then
    xx=$( echo $line | cut -d' ' -f2 )
    yy=$( echo $line | cut -d' ' -f3 )
    zz=$( echo $line | cut -d' ' -f4 )
    rr=$( echo " sqrt( ($xx-$x)^2 + ($yy-$y)^2 + ($zz-$z)^2 ) " | bc -l)
    if [[ $( echo " $rr < $min " | bc -l ) == 1 ]]
    then
      if [[ $( echo " $rr > 0.0 " | bc -l) == 1 ]]
      then
        min=$rr
        echo $min
      fi
    fi
  fi
done < geometry_z_dir.bohr.xyz
