#!/bin/bash

dimer_axis=z

scale_factor_bohr_ang=0.5291772106712

effective_diameter=49.314774411

if [[ $dimer_axis == "x" ]]
then
  axis_number=1
  column_number=2
elif [[ $dimer_axis == "y" ]]
then
  axis_number=2
  column_number=3
elif [[ $dimer_axis == "z" ]]
then
  axis_number=3
  column_number=4
fi

for ii in $( ls .)
do
  if [[ $ii = *bohr*.xyz ]]
  then
    geo_file_bohr=$ii
    line_idx=0
    atom_idx=0
    sphere_idx[1]=0
    sphere_idx[2]=0
    sphere_atom_number[1]=0
    sphere_atom_number[2]=0
    sphere_x[1]=0
    sphere_x[2]=0
    sphere_y[1]=0
    sphere_y[2]=0
    sphere_z[1]=0
    sphere_z[2]=0
    radius[1]=0
    radius[2]=0
    while read geo_file_line
    do
      line_idx=$( echo " $line_idx + 1 " | bc)
      if [[ $line_idx -gt 2 ]]
      then
        atom_idx=$(   echo "$atom_idx+1"  | bc )
        atom_name[$atom_idx]=$(  echo $geo_file_line | cut -d ' ' -f 1)
        geo_x_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 2)
        geo_y_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 3)
        geo_z_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 4)
        if [[ $(echo " $( echo $geo_file_line | cut -d ' ' -f $column_number ) > 0.0 " | bc -l) -eq 1 ]]
        then
          sphere_idx[$atom_idx]=1
          sphere_atom_number[1]=$( echo " ${sphere_atom_number[1]} + 1 " | bc )
          sphere_x[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_x[${sphere_idx[$atom_idx]}]} + ${geo_x_bohr[$atom_idx]} " | bc -l )
          sphere_y[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_y[${sphere_idx[$atom_idx]}]} + ${geo_y_bohr[$atom_idx]} " | bc -l )
          sphere_z[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_z[${sphere_idx[$atom_idx]}]} + ${geo_z_bohr[$atom_idx]} " | bc -l )
        elif [[ $(echo " $( echo $geo_file_line | cut -d ' ' -f $column_number ) < 0.0 " | bc -l) -eq 1 ]]
        then
          sphere_idx[$atom_idx]=2
          sphere_atom_number[2]=$( echo " ${sphere_atom_number[2]} + 1 " | bc )
          sphere_x[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_x[${sphere_idx[$atom_idx]}]} + ${geo_x_bohr[$atom_idx]} " | bc -l )
          sphere_y[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_y[${sphere_idx[$atom_idx]}]} + ${geo_y_bohr[$atom_idx]} " | bc -l )
          sphere_z[${sphere_idx[$atom_idx]}]=$( echo " ${sphere_z[${sphere_idx[$atom_idx]}]} + ${geo_z_bohr[$atom_idx]} " | bc -l )
        fi
        #echo $axis_number ${sphere_idx[$atom_idx]} $geo_x_bohr $geo_y_bohr $geo_z_bohr
      fi
    done < $geo_file_bohr
    n_atoms=$atom_idx
    sphere_x[1]=$( echo " ${sphere_x[1]} / ${sphere_atom_number[1]} " | bc -l )
    sphere_x[2]=$( echo " ${sphere_x[2]} / ${sphere_atom_number[2]} " | bc -l )
    sphere_y[1]=$( echo " ${sphere_y[1]} / ${sphere_atom_number[1]} " | bc -l )
    sphere_y[2]=$( echo " ${sphere_y[2]} / ${sphere_atom_number[2]} " | bc -l )
    sphere_z[1]=$( echo " ${sphere_z[1]} / ${sphere_atom_number[1]} " | bc -l )
    sphere_z[2]=$( echo " ${sphere_z[2]} / ${sphere_atom_number[2]} " | bc -l )
    for ii in $( seq 1 $n_atoms )
    do
      dx=$( echo " ${geo_x_bohr[$ii]} - ${sphere_x[${sphere_idx[$ii]}]} " | bc -l )
      dy=$( echo " ${geo_y_bohr[$ii]} - ${sphere_y[${sphere_idx[$ii]}]} " | bc -l )
      dz=$( echo " ${geo_z_bohr[$ii]} - ${sphere_z[${sphere_idx[$ii]}]} " | bc -l )
      rr=$( echo " sqrt( $dx^2 + $dy^2 + $dz^2 ) " | bc -l )
      if [[ $( echo " ${radius[${sphere_idx[$ii]}]} < $rr " | bc -l ) -eq 1 ]]
      then
        radius[${sphere_idx[$ii]}]=$rr
      fi
    done
    diameter[1]=$( echo " 2 * ${radius[1]} " | bc -l )
    diameter[2]=$( echo " 2 * ${radius[2]} " | bc -l )
    mean_diameter=$( echo " ( ${diameter[1]} + ${diameter[2]} ) / 2 " | bc -l )
    if [[ $dimer_axis == "x" ]]
    then
      bb=$( echo " ${sphere_x[1]} - ${sphere_x[2]} " | bc -l )
    elif [[ $dimer_axis == "y" ]]
    then
      bb=$( echo " ${sphere_y[1]} - ${sphere_y[2]} " | bc -l )
    elif [[ $dimer_axis == "z" ]]
    then
      bb=$( echo " ${sphere_z[1]} - ${sphere_z[2]} " | bc -l )
    fi
    dd=$( echo " $bb - $mean_diameter " | bc -l )
    dd_effective=$( echo " $bb - $effective_diameter " | bc -l )
    echo 'sphere 1 center coordinates  :' ${sphere_x[1]} '|' ${sphere_y[1]} '|' ${sphere_z[1]}
    echo 'sphere 1 diameter            :' ${diameter[1]}
    echo 'sphere 2 center coordinates  :' ${sphere_x[2]} '|' ${sphere_y[2]} '|' ${sphere_z[2]}
    echo 'sphere 2 diameter            :' ${diameter[2]}
    echo 'average sphere diameter      :' $mean_diameter
    echo 'effective sphere diameter    :' $effective_diameter
    echo 'distance of sphere centers   :' $bb
    echo 'direct distance of spheres   :' $dd
    echo 'effective distance of spheres:' $dd_effective
  fi
done
