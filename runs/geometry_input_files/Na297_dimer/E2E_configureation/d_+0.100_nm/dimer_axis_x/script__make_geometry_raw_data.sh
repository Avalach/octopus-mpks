#!/bin/bash

dimer_axis=z

scale_factor_bohr_ang=0.5291772106712

if [[ $dimer_axis == "x" ]]
then
  axis_number=1
  column_number=2
elif [[ $dimer_axis == "y" ]]
then
  axis_number=2
  column_number=3
elif [[ $dimer_axis == "z" ]]
then
  axis_number=3
  column_number=4
fi

for ii in $( ls .)
do
  if [[ $ii = *bohr*.xyz ]]
  then
    geo_file_bohr=$ii
    geo_file_raw=raw_data_x_dir.bohr.xyz
    if [[ ! -e $geo_file_raw ]]
    then
      line_idx=0
      atom_idx=0
      sphere_idx[1]=0
      sphere_idx[2]=0
      sphere_atom_number[1]=0
      sphere_atom_number[2]=0
      sphere_x[1]=0
      sphere_x[2]=0
      sphere_y[1]=0
      sphere_y[2]=0
      sphere_z[1]=0
      sphere_z[2]=0
      radius[1]=0
      radius[2]=0
      while read geo_file_line
      do
        line_idx=$( echo " $line_idx + 1 " | bc)
        if [[ $line_idx -lt 2 ]]
        then
          echo '  '$geo_file_line >> $geo_file_raw
        elif [[ $line_idx -eq 2 ]]
        then
          echo '  '$( echo $geo_file_line | cut -d' ' -f1) a >> $geo_file_raw
        elif [[ $line_idx -gt 2 ]]
        then
          atom_idx=$(   echo "$atom_idx+1"  | bc )
          atom_name[$atom_idx]=$(  echo $geo_file_line | cut -d ' ' -f 1)
          geo_x_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 2)
          geo_y_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 3)
          geo_z_bohr[$atom_idx]=$( echo $geo_file_line | cut -d ' ' -f 4)
          if [[ $(echo " $( echo $geo_file_line | cut -d ' ' -f $column_number ) >= 0.0 " | bc -l) -eq 1 ]]
          then
            sphere_idx[$atom_idx]=1
            sphere_atom_number[1]=$( echo " ${sphere_atom_number[1]} + 1 " | bc )
            sphere_x[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_x[${sphere_idx[$atom_idx]}]} + ${geo_x_bohr[$atom_idx]} " | bc -l )
            sphere_y[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_y[${sphere_idx[$atom_idx]}]} + ${geo_y_bohr[$atom_idx]} " | bc -l )
            sphere_z[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_z[${sphere_idx[$atom_idx]}]} + ${geo_z_bohr[$atom_idx]} " | bc -l )
          elif [[ $(echo " $( echo $geo_file_line | cut -d ' ' -f $column_number ) < 0.0 " | bc -l) -eq 1 ]]
          then
            sphere_idx[$atom_idx]=2
            sphere_atom_number[2]=$( echo " ${sphere_atom_number[2]} + 1 " | bc )
            sphere_x[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_x[${sphere_idx[$atom_idx]}]} + ${geo_x_bohr[$atom_idx]} " | bc -l )
            sphere_y[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_y[${sphere_idx[$atom_idx]}]} + ${geo_y_bohr[$atom_idx]} " | bc -l )
            sphere_z[${sphere_idx[$atom_idx]}]=$( echo " scale=6; ${sphere_z[${sphere_idx[$atom_idx]}]} + ${geo_z_bohr[$atom_idx]} " | bc -l )
          fi
          #echo $axis_number ${sphere_idx[$atom_idx]} $geo_x_bohr $geo_y_bohr $geo_z_bohr
        fi
      done < $geo_file_bohr
      n_atoms=$atom_idx
      sphere_x[1]=$( echo " scale=6; ${sphere_x[1]} / ${sphere_atom_number[1]} " | bc -l )
      sphere_x[2]=$( echo " scale=6; ${sphere_x[2]} / ${sphere_atom_number[2]} " | bc -l )
      sphere_y[1]=$( echo " scale=6; ${sphere_y[1]} / ${sphere_atom_number[1]} " | bc -l )
      sphere_y[2]=$( echo " scale=6; ${sphere_y[2]} / ${sphere_atom_number[2]} " | bc -l )
      sphere_z[1]=$( echo " scale=6; ${sphere_z[1]} / ${sphere_atom_number[1]} " | bc -l )
      sphere_z[2]=$( echo " scale=6; ${sphere_z[2]} / ${sphere_atom_number[2]} " | bc -l )
      for ii in $( seq 1 $n_atoms )
      do
        geo_x_bohr_new[$ii]=$( echo " scale=6; ${geo_x_bohr[$ii]} - ${sphere_x[${sphere_idx[$ii]}]} " | bc -l )
        geo_y_bohr_new[$ii]=$( echo " scale=6; ${geo_y_bohr[$ii]} - ${sphere_y[${sphere_idx[$ii]}]} " | bc -l )
        geo_z_bohr_new[$ii]=$( echo " scale=6; ${geo_z_bohr[$ii]} - ${sphere_z[${sphere_idx[$ii]}]} " | bc -l )
        printf '  %s           % 11.7f    % 11.7f    % 11.7f  \n'  "${atom_name[$ii]}" "${geo_x_bohr_new[$ii]}" "${geo_y_bohr_new[$ii]}" "${geo_z_bohr_new[$ii]}" >> $geo_file_raw
      done
    fi
  fi
done
