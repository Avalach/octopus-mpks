#!/bin/bash

scale_factor=1.8897261254535431
for ii in $( ls .)
do
  if [[ $ii = *angstrom*.xyz ]]
  then
    geo_file_ang=$ii
    geo_file_bohr=$( echo $geo_file_ang | cut -d'.' -f1)".bohr."$( echo $geo_file_ang | cut -d'.' -f3)
    if [[ ! -e $geo_file_bohr ]]
    then
      line_idx=0
      while read geo_file_line
      do
        line_idx=$( echo " $line_idx + 1 " | bc)
        if [[ $line_idx -lt 2 ]]
        then
          echo '  '$geo_file_line >> $geo_file_bohr
        elif [[ $line_idx -eq 2 ]]
        then
          echo '  '$( echo $geo_file_line | cut -d' ' -f1) b >> $geo_file_bohr
        elif [[ $line_idx -gt 2 ]]
        then
          atom_name=$( echo $geo_file_line | cut -d ' ' -f 1)
          geo_x_ang=$( echo $geo_file_line | cut -d ' ' -f 2)
          geo_x_bohr=$(  echo " $geo_x_ang * $scale_factor " | bc -l)
          geo_x_bohr=$( printf "%08.6f" "$geo_x_bohr" )
          geo_y_ang=$( echo $geo_file_line | cut -d ' ' -f 3)
          geo_y_bohr=$(  echo " $geo_y_ang * $scale_factor " | bc -l)
          geo_y_bohr=$( printf "%08.6f" "$geo_y_bohr" )
          geo_z_ang=$( echo $geo_file_line | cut -d ' ' -f 4)
          geo_z_bohr=$(  echo " $geo_z_ang * $scale_factor " | bc -l)
          geo_z_bohr=$( printf "%08.6f" "$geo_z_bohr" )
          if [[ $(echo " $geo_x_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_bohr}'   '${geo_y_bohr}'   '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_bohr}'   '${geo_y_bohr}'    '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_bohr}'    '${geo_y_bohr}'   '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_bohr}'   '${geo_y_bohr}'   '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                  '${geo_x_bohr}'    '${geo_y_bohr}'    '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr < 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_bohr}'   '${geo_y_bohr}'    '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr < 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_bohr}'    '${geo_y_bohr}'   '${geo_z_bohr} >> $geo_file_bohr
          elif [[ $(echo " $geo_x_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_y_bohr >= 0.0 " | bc -l) -eq 1 ]] && [[ $(echo " $geo_z_bohr >= 0.0 " | bc -l) -eq 1 ]]
          then
            echo '      '${atom_name}'                   '${geo_x_bohr}'    '${geo_y_bohr}'    '${geo_z_bohr} >> $geo_file_bohr
          fi
        fi
      done < $geo_file_ang
    fi
  fi
done
