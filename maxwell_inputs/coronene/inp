
# input template for maxwell_ks runs with forward and backward Maxwell to matter coupling

# gs           : groundstate calculation, only matter variables for groundstate calculation are necessary
# td           : time-dependent calculation, only matter variables and time variables are necessary
# maxwell_ks   : time-dependent calculation, matter variables, Maxwell variables and time variables are necessary
# maxwell_free : time-dependent calculation, only Maxwell variables and time variables are necessary


# ----- Calculation mode variables ----------------------------------------------------------------

# ----- general calculation (always used): 

 Debug                               = trace

 ProfilingMode                       = yes

 CalculationMode                     = maxwell_ks

 ParDomains                          = auto
 ParStates                           = no
 MaxwellParDomains                   = auto
 MaxwellParStates                    = no



# ----- Matter box variables ----------------------------------------------------------------------

# ----- general calculation (not used for maxwell_free):  

 Dimensions                          = 3
 BoxShape                            = minimum

 Radius                              = 16.000

# %Lsize
#  000021.000 | 000015.000 | 000021.000
# %

 %Spacing
  000000.400 | 000000.400 | 000000.400
 %



# ----- Maxwell box variables ---------------------------------------------------------------------

# ----- general calculation (not used for "gs"):

 MaxwellDimensions                   = 3
 MaxwellBoxShape                     = parallelepiped

 %MaxwellLsize
  000028.000 | 000016.000 | 000028.000
 %
 
 %MaxwellSpacing
  000000.400 | 000000.400 | 000000.400
 %



# ----- Species variables -------------------------------------------------------------------------

# ----- general calculation (not used for "maxwell_free"):

## %Species
##  'C' | species_pseudo | file | 'C.psf'
## %

# %Species
#  'C' | species_pseudo | file | 'C.psf'
#  'H' | species_pseudo | file | 'H.psf'
# %

## %Species
##  'C' | species_pseudo | file | 'C.psf'
##  'H' | species_pseudo | file | 'H.psf'
##  '' | species_pseudo | file | ''
## %

## %Species
##  'C' | species_pseudo | file | 'C.psf'
##  'H' | species_pseudo | file | 'H.psf'
##  '' | species_pseudo | file | ''
##  '' | species_pseudo | file | ''
## %

## %Species
##  'C' | species_pseudo | file | 'C.psf'
##  'H' | species_pseudo | file | 'H.psf'
##  '' | species_pseudo | file | ''
##  '' | species_pseudo | file | ''
##  '' | species_pseudo | file | ''
## %

 XYZCoordinates                      = "coronene.geometry_z_dir.bohr.xyz"
 UnitsXYZFiles                       = octopus_units



# ----- Matter calculation variables --------------------------------------------------------------

# ----- general calculation (not used for "maxwell_free"):

 ExtraStates                         = 000
 XCFunctional                        = lda_x + lda_c_gl
 DerivativesStencil                  = stencil_starplus
 DerivativesOrder                    = 4
 SymmetriesCompute                   = no
 SmearingFunction                    = semiconducting
 Smearing                            = 1e-0
 MixingScheme                        = broyden
 Mixing                              = 0.3
 DisableOpenCl                       = yes
 FilterPotentials                    = filter_ts


# ----- ground state calculation:

# EigenSolver                         = cg
# EigenSolverTolerance                = 1.00e-12
# EigensolverMaxIter                  = 10
# ConvEnergy                          = 1.00e-12
# ConvRelDens                         = 1.00e-12
# ConvForce                           = 0.0
# MaximumIter                         = 2000
# LCAOStart                           = lcao_states


# ----- time-dependent calculation:

 TDExpOrder                          = 4
 AbsorbingBoundaries                 = mask
 ABWidth                             = 4.000
 MoveIons                            = no
 TDMultipoleLmax                     = 2



# ----- Maxwell calculation variables -------------------------------------------------------------

# ----- time-dependent calculation:

 MaxwellHamiltonianOperator          = faraday_ampere
 MaxwellTDOperatorMethod             = maxwell_op_fd
 MaxwellTDPropagator                 = maxwell_etrs
 MaxwellDerivativesStencil           = stencil_starplus
 MaxwellDerivativesOrder             = 4
 MaxwellNLOperatorCompactBoundaries  = yes
 MaxwellTDExpOrder                   = 4
 MatterToMaxwellCoupling             = yes
 MaxwellToMatterCoupling             = yes
 MaxwellCouplingMethod               = multipole_expansion_center_of_mass
 MaxwellCouplingOrder                = electric_dipole_coupling + magnetic_dipole_coupling + electric_quadrupole_coupling 
 MaxwellTransFieldCalculationMethod  = trans_field_poisson
 MaxwellPoissonSolverBoundaries      = multipole
 MaxwellLorentzForce                 = no
 MaxwellDiamagneticCurrent           = no
 MaxwellSpinCurrent                  = no
 MaxwellDiamagneticCurrent           = no
 MaxwellSpinCurrent                  = no
 
 %MaxwellBoundaryConditions
  maxwell_constant | maxwell_constant | maxwell_constant
 %

 %MaxwellAbsorbingBoundaries
  cpml | cpml | cpml
 %

 MaxwellABMaskWidth                  = 4.000
 MaxwellABPMLWidth                   = 4.000
 MaxwellABPMLKappaMax                = 1.000
 MaxwellABPMLAlphaMax                = 0.000
 MaxwellABPMLPower                   = 2.000
 MaxwellABPMLReflectionError         = 1e-16



# ----- Output format -----------------------------------------------------------------------------

 OutputFormat                        = plane_x + plane_y + plane_z + vtk + xyz



# ----- Matter output variables -------------------------------------------------------------------

# ----- general calculation:

 Output                              = density + potential + current + geometry + forces + elf + pol_density + matrix_elements


# ----- time-dependent calculation:

 OutputInterval                      = 50
 TDOutput                            = energy + geometry + multipoles + laser



# ----- Maxwell output variables ------------------------------------------------------------------

# ----- general calculation:

 MaxwellOutput                       = maxwell_electric_field + maxwell_magnetic_field + maxwell_trans_electric_field + maxwell_long_electric_field + maxwell_energy_density + maxwell_poynting_vector + maxwell_charge_density 


# ----- time-dependent calculation:

 MaxwellOutputInterval               = 50
 MaxwellTDOutput                   = maxwell_energy + maxwell_fields + maxwell_mean_poynting + maxwell_spectrum_energy + maxwell_poynting_surface + maxwell_e_field_surface + maxwell_b_field_surface



# ----- Time step variables -----------------------------------------------------------------------

# ----- general calculation:

 TDTimeStep                          = 0.0421325
 TDMaxSteps                          = 237346
 TDEnergyUpdateIter                  = 1


# ----- Maxwell calculation:

 TDMaxwellTDRelaxationSteps          = 118
 TDMaxwellKSRelaxationSteps          = 237
 MaxwellTDIntervalSteps              = 25
 MaxwellTDETRSApprox                 = const_steps



# ----- Maxwell field variables -------------------------------------------------------------------

# ----- physical constants:

 pi=3.14159265359
 c=137.035999679



# ----- laser propagates in x direction:

# lambda      = 1000000.0000
# omega       =  0.000861022580
# kx          =  0.000006283185
# Ez          =  0.010000000000
# pulse_width = 10000.0000
# pulse_shift =  6000.0000
# pulse_slope =   250.0000

# %UserDefinedMaxwellIncidentWaves
#  plane_waves_parser | "kx" | "0"  | "0" | "0" | "0" | "Ez*cos(kx*(x-pulse_shift))*exp(-((x-pulse_shift)^2/(2*pulse_width^2)))" | 
# %

# %UserDefinedMaxwellIncidentWaves
#   plane_waves_mx_function | 0 | 0 | Ez | "wave_function" | plane_wave
# %

# %MaxwellFunctions
#  "wave_function" | mxf_cosinoidal_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | pulse_width 
# %

  # const plane wave
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field |        " Ez*cos(kx*(x-pulse_shift)) "
#  2 | formula | magnetic_field | " -1/c * Ez*cos(kx*(x-pulse_shift)) "
# %

  # gaussian plane wave
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "       Ez*cos(kx*(x-pulse_shift)) * exp(-((x-pulse_shift)^2 / (2*pulse_width^2))) "
#  2 | formula | magnetic_field | " -1/c* Ez*cos(kx*(x-pulse_shift)) * exp(-((x-pulse_shift)^2 / (2*pulse_width^2))) "
# %

  # cosinoidal plane wave
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "      Ez * cos(kx*(x-pulse_shift)) * cos(pi/2*(x-2*pulse_width-pulse_shift)/pulse_width+pi) * step(pulse_width-abs((kx*x-kx*pulse_shift)/sqrt(kx^2)) ) "
#  2 | formula | magnetic_field | " -1/c*Ez * cos(kx*(x-pulse_shift)) * cos(pi/2*(x-2*pulse_width-pulse_shift)/pulse_width+pi) * step(pulse_width-abs((kx*x-kx*pulse_shift)/sqrt(kx^2)) ) "
# %

 # logistic pulse
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "      Ez*cos(kx*(x-pulse_shift)) * ( 1 / (1+exp(pulse_slope*(x-pulse_shift-pulse_width/2))) + 1 / (1+exp(-pulse_slope*(x-pulse_shift+pulse_width/2))) - 1 ) "
#  2 | formula | magnetic_field | " -1/c*Ez*cos(kx*(x-pulse_shift)) * ( 1 / (1+exp(pulse_slope*(x-pulse_shift-pulse_width/2))) + 1 / (1+exp(-pulse_slope*(x-pulse_shift+pulse_width/2))) - 1 ) "
# %

 # trapezoidal pulse
# psh = pulse_shift
# pwi = pulse_width
# psl = pulse_slope
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "      Ez*cos(kx*(x-psh)) * ((1-psl*(x-psh-pwi/2)*step(x-psh-pwi/2))*step(-(x-psh)+pwi/2+1/psl) + (-1+psl*(x-psh+pwi/2+1/psl)*step(x-psh+pwi/2+1/psl))*step(-(x-psl)-pwi/2)) "
#  2 | formula | magnetic_field | " -1/c*Ez*cos(kx*(x-psh)) * ((1-psl*(x-psh-pwi/2)*step(x-psh-pwi/2))*step(-(x-psh)+pwi/2+1/psl) + (-1+psl*(x-psh+pwi/2+1/psl)*step(x-psh+pwi/2+1/psl))*step(-(x-psl)-pwi/2)) "
# %

 # twisted wave
# waist at focal plane
# w0 =     0.0000
# wx =     0.0000
# wy =     0.0000
# wz =     0.0000
# initial phase
# t0 =     0.0000
# Rayleigh length
# Zr = pi*w0^2/lambda
# TEM(mm,nn) mode
# mm = 00
# nn = 00
# pulse shift
# dx =  6000.0000
# %UserDefinedMaxwellStates
#   3 | formula | electric_field | "     0.5*Ez/sqrt(1+(x-dx)^2/Zr^2)*hermite(mm, z)*hermite(nn, y)*exp(-(z^2+y^2)/(w0^2*(1+(x-dx)^2/Zr^2)))*cos(kx*(x-dx)+kx/(1+(x-dx)^2/Zr^2)*(z^2+y^2)/Zr^2-(mm+nn+1)*atan((x-dx)/Zr)+t0)"
#   2 | formula | magnetic_field | "-1/c*0.5*Ez/sqrt(1+(x-dx)^2/Zr^2)*hermite(mm, z)*hermite(nn, y)*exp(-(z^2+y^2)/(w0^2*(1+(x-dx)^2/Zr^2)))*cos(kx*(x-dx)+kx/(1+(x-dx)^2/Zr^2)*(z^2+y^2)/Zr^2-(mm+nn+1)*atan((x-dx)/Zr)+t0)"
# %



# ----- spatial constant field propagation

 Ez          =  0.010000000000
 By          =  0.010000000000
 pulse_width = 10000.0000
 pulse_shift =  6000.0000
 pulse_slope =   250.0000

# ----- constant electric field
# %UserDefinedConstantSpacialMaxwellField
#   0 | 0 | Ez | 0 | 0 | 0 | "time_function"
# %

# %TDFunctions
#  "time_function" | tdf_ramp | 1.0 | pulse_slope 
# %

# ----- constant magnetic field
 %UserDefinedConstantSpacialMaxwellField
   0 | 0 | 0 | 0 | By | 0 | "time_function"
 %

 %TDFunctions
  "time_function" | tdf_ramp | 1.0 | pulse_slope 
 %



# ----- external current ------------------------

# MaxwellExternalCurrent              = no

# MaxwellConstantsEqualOne            = 

# jomega = 
# t0     = 
# js     = 
# jmax   = 

# %UserDefinedMaxwellExternalCurrent
#  external_current_td_function |  "0" | "0" | " exp(-x^2/2) * exp(-y^2/2) " | jomega | "external_current_envelope_function"
# %

# %TDFunctions
#  "external_current_envelope_function" | tdf_gaussian  | jmax | js | t0
# %


# ----- internal current test -------------------

#  CurrentPropagationTest              = no



# ----- classical external field for td run -------------------------------------------------------

# lambda= 1000000.0000
# omega =  0.000861022580
# Ez    =  0.010000000000
# t0    = 
# tau0  = 

# %TDExternalFields
#   electric_field | 0 | 0 | 1 | omega | "envelope_gauss"
# %

# %TDFunctions
#   "envelope_gauss" | tdf_gaussian |  0.010000000000 | tau0 | t0
# %



# ----- delta kick --------------------------------------------------------------------------------

# TDDeltaStrength                      = 
# TDPolarizationDirection              = 



