#!/bin/bash

pushd ./maxwell_e_field/maxwell_e_field-z.z=0/
  ./script_maxwell_e_field-z_figures.z=0.sh
popd

pushd ./maxwell_b_field/maxwell_e_field-y.z=0/
  ./script_maxwell_b_field-y_figures.z=0.sh
popd
