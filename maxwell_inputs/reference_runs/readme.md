# Octopus reference runs for coupled Maxwell-Kohn-Sham systems


<br />



## Matter groundstate calculation


### Sodium moelcule

00_groundstate.01_Na2


### Benzene molecule

00_groundstate.02_benzene


<br />



## Only Maxwell propagation


### Cosinoidal plane wave pulse passing the simulation box

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.01_no_medium.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.01_no_medium.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.01_no_medium.03_pml  


### Cosinoidal plane wave pulse hitting a linear medium box

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.02_medium_box.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.02_medium_box.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.01_cosinoidal_plane_wave.02_medium_box.03_pml  


### Gaussian plane wave with no medium passing the simulation box

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.02_gaussian_plane_wave.01_no_medium.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.02_gaussian_plane_wave.01_no_medium.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.02_gaussian_plane_wave.01_no_medium.03_pml  


### Two cosinoidal plane waves with different wavevectors interfere in the simulation box

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.03_two_cosinoidal_plane_waves.01_no_medium.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.03_two_cosinoidal_plane_waves.01_no_medium.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.03_two_cosinoidal_plane_waves.01_no_medium.03_pml  


### Gaussian shaped spatial distribution of an external current density passed by a Gaussian temporal current pulse

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.04_external_temporal_gaussian_current.01_no_medium.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.04_external_temporal_gaussian_current.01_no_medium.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.04_external_temporal_gaussian_current.01_no_medium.03_pml  


### Gaussian shaped spatial distribution of an external current density passed by a Gaussian temporal current pulse plus Cosinoidal shaped plane wave pulse passing the box

** No absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.05_external_temporal_gaussian_current_and_cosinoidal_plane_wave.01_no_medium.01_no_ab  

** Mask absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.05_external_temporal_gaussian_current_and_cosinoidal_plane_wave.01_no_medium.02_mask  

** Pml absorbing boundaries: **  
&nbsp;&nbsp;&nbsp;
01_mx_propagation.05_external_temporal_gaussian_current_and_cosinoidal_plane_wave.01_no_medium.03_pml  


<br />



## Maxwell matter propagation:


### Cosinoidal plane wave pulse hitting a molecule

** Cosinoidal plane wave pulse hitting a molecule without Maxwell-matter backreaction in dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.01_cosinoidal_plane_wave.01_ed_coupling.01_no_backreaction.03_pml  

** Cosinoidal plane wave pulse hitting a molecule without Maxwell-matter backreaction beyond dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.01_cosinoidal_plane_wave.02_ed_md_eq_coupling.01_no_backreaction.03_pml  

** Cosinoidal plane wave pulse hitting a molecule with Maxwell-matter backreaction in dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.01_cosinoidal_plane_wave.01_ed_coupling.02_backreaction.03_pml  

** Cosinoidal plane wave pulse hitting a molecule with Maxwell-matter backreaction beyond dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.01_cosinoidal_plane_wave.01_ed_md_eq_coupling.02_backreaction.03_pml


### Instantaneous magnetic field increasing

** Logistic instantaneous magnetic field increasing in time without Maxwell-matter backreaction in dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.02_instantaneous_field_increasing.01_ed_coupling.01_no_backreaction.03_pml  

** Logistic instantaneous magnetic field increasing in time without Maxwell-matter backreaction beyond dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.02_instantaneous_field_increasing.02_ed_md_eq_coupling.01_no_backreaction.03_pml  

** Logistic instantaneous magnetic field increasing in time without Maxwell-matter backreaction in dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.02_instantaneous_field_increasing.01_ed_coupling.02_backreaction.03_pml

** Logistic instantaneous magnetic field increasing in time with Maxwell-matter backreaction beyond dipole approximation **  
&nbsp;&nbsp;&nbsp;
02_mx_ma_propagation.02_instantaneous_field_increasing.02_ed_md_eq_coupling.02_backreaction.03_pml
