# input file for maxwell_ks run with gaussian laser pulse
 
 Debug                             = trace_file

 ProfilingMode                     = yes

 CalculationMode                   = maxwell_ks


 ParDomains                        = auto
 ParStates                         = no
 MaxwellParDomains                 = auto
 MaxwellParStates                  = no

 pi=3.14159265359
 c=137.035999679


# ----- Matter box variables ----------------------------------------------------------------------
 lsize_ma = 10.0
 dx_ma = 0.5

 Dimensions                        = 3
 BoxShape                          = parallelepiped
 %Lsize
  lsize_ma | lsize_ma | lsize_ma
 %
 %Spacing
  dx_ma | dx_ma | dx_ma
 %


# ----- Maxwell box variables ---------------------------------------------------------------------
 lsize_mx = 17.0  # free maxwell box limit of 10.0 plus for the 5.0 absorbing pml boundaries and plus 2.0 for the incident wave boundaries with der_order = 4 times dx_mx 
 dx_mx = 0.5

 MaxwellDimensions                 = 3
 MaxwellBoxShape                   = parallelepiped

 %MaxwellLsize
  lsize_mx | lsize_mx | lsize_mx
 %

 %MaxwellSpacing
  dx_mx | dx_mx | dx_mx
 %


# ----- Species variables -------------------------------------------------------------------------

# %Species
# 'Na'       | species_pseudo         | file | '/home/jestaedt/maxwell_tdks/octopus_maxwell_svn-15751/share/octopus/pseudopotentials/PSF//Na.psf'
# %

# %Coordinates
#  'Na' | 0 | 0 | -1.53945
#  'Na' | 0 | 0 |  1.53945
# %

 XYZCoordinates                    = "Na2.geometry_z_dir.bohr.xyz"


# ----- Matter calculation variables --------------------------------------------------------------

 Extrastates                       = 2

# XCFunctional                     = gga_x_pbe_r + gga_c_pbe
 XCFunctional                      = lda_x + lda_c_gl

 EigenSolver                       = cg
 EigenSolverTolerance              = 1.00e-12
 EigensolverMaxIter                = 50
 ConvRelDens                       = 1.00e-12
 MaximumIter                       = 1000

 DerivativesStencil                = stencil_starplus
 DerivativesOrder                  = 4
 TDExpOrder                        = 4

# SymmetriesCompute                 = no
# ConvForce                         = 0.0
# SmearingFunction                  = fermi_dirac
# Smearing                          = 1/1000
# MixingScheme                      = broyden
# Mixing                            = 0.02
# LCAOStart                         = lcao_states
# DisableOpenCl                     = yes
# FilterPotentials                  = filter_ts

# MoveIons                          = no

 AbsorbingBoundaries               = not_absorbing
# ABWidth                           = 2.0


# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator        = faraday_ampere
 MaxwellTDOperatorMethod           = maxwell_op_fd
 MaxwellTDPropagator               = maxwell_etrs

 MatterToMaxwellCoupling           = yes
 MaxwellToMatterCoupling           = yes
 MaxwellCouplingOrder              = electric_dipole_coupling + magnetic_dipole_coupling + electric_quadrupole_coupling
 MaxwellTransFieldCalculationMethod= trans_field_poisson
 MaxwellTransFieldCalculationCorr  = no
 MaxwellPoissonSolver              = isf
 MaxwellPoissonSolverBoundaries    = multipole
 MaxwellDiamagneticCurrent         = yes

# MaxwellVacuumFluctuation          = no

 MaxwellDerivativesStencil         = stencil_starplus
 MaxwellDerivativesOrder           = 4
 MaxwellTDExpOrder                 = 4

 %MaxwellBoundaryConditions
  maxwell_plane_waves | maxwell_plane_waves | maxwell_plane_waves
 %

 %MaxwellAbsorbingBoundaries
  cpml | cpml | cpml
 %

# MaxwellABMaskWidth                = 5.0

 MaxwellABPMLWidth                 = 5.0
 MaxwellABPMLKappaMax              = 1.0
 MaxwellABPMLAlphaMax              = 1.0
 MaxwellABPMLPower                 = 2.0
 MaxwellABPMLReflectionError       = 1e-16


# ----- Output variables --------------------------------------------------------------------------

 OutputFormat                      = plane_x + plane_y + plane_z + vtk + xyz + axis_x  


# ----- Matter output variables -------------------------------------------------------------------

 Output                            = potential + density + current + geometry + forces + elf
 OutputInterval                    = 50
 TDOutput                          = energy + multipoles + laser + geometry


# ----- Maxwell output variables ------------------------------------------------------------------

# MaxwellOutput                     = electric_field + magnetic_field + trans_electric_field + long_electric_field + maxwell_energy_density 
 MaxwellOutput                     = maxwell_electric_field + maxwell_magnetic_field + maxwell_energy_density + maxwell_trans_electric_field 
 MaxwellOutputInterval             = 1
 MaxwellTDOutput                   = maxwell_energy + maxwell_fields

 %MaxwellFieldsCoordinate
   0.00 | 0.00 | 0.00
 %

# ----- Time step variables -----------------------------------------------------------------------


# TDTimeStep                        = 1 / ( sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2) )
 TDTimeStep                        = 0.002
 TDMaxSteps                        = 200
 TDEnergyUpdateIter                = 1
 MaxwellTDIntervalSteps            = 1

 TDMaxwellTDRelaxationSteps        = 0
 TDMaxwellKSRelaxationSteps        = 0
 MaxwellTDETRSApprox               = no

 CurrentPropagationTest            = no


# ----- Maxwell field variables -------------------------------------------------------------------

# ----- laser propagates in x direction ---------
 lambda      = 10.0
 omega       = 2 * pi * c / lambda
 kx          = omega / c
 Ez          = 0.05
 pulse_width = 10.0
 pulse_shift =  - 5 * 5.0
 phi         = 0
 gr          = 0.3

# %UserDefinedMaxwellIncidentWaves
#  plane_waves_parser | " kx " | " 0 " | " 0 " | " 0 " | " 0 " | " Ez * cos(kx*(x-dx)) * exp(-((x-dx)/sx)^2)| plane_wave
# %

 %UserDefinedMaxwellIncidentWaves
   plane_wave_mx_function | 0 | 0 | Ez | "plane_wave_function"
 %

 %MaxwellFunctions
   "plane_wave_function" | mxf_cosinoidal_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | pulse_width
 %

# %MaxwellFunctions
#   "plane_waves_function" | mxf_logistic_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | gr | pulse_width
# %

# %MaxwellFunctions
#   "plane_waves_function" | mxf_trapezoidal_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | gr | pulse_width
# %

 %UserDefinedInitialMaxwellStates
  3 | mx_function | electric_field |  Ez   | "plane_wave_function"
  2 | mx_function | magnetic_field | -Ez/c | "plane_wave_function"
 %

 # gaussian pulse
# %UserDefinedInitialMaxwellStates
#  2 | formula | magnetic_field | " 1 / ( 1 + 9999 * exp(-800*t)) "
# %

 # constant pulse
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "       Ez*cos(kx*(x-pulse_shift)) "
#  2 | formula | magnetic_field | " - 1/c*Ez*cos(kx*(x-pulse_shift)) "
# %

 # gaussian pulse 
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "      Ez*cos(kx*(x-pulse_shift))*exp(-((x-pulse_shift)^2/(2*pulse_width^2))) "
#  2 | formula | magnetic_field | " -1/c*Ez*cos(kx*(x-pulse_shift))*exp(-((x-pulse_shift)^2/(2*pulse_width^2))) "   
# %

 # cosinoidal pulse
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "        Ez * cos( kx * ( x - pulse_shift ) ) * cos( pi/2 * ( x - pulse_shift - 2*pulse_width ) / pulse_width ) * step( pulse_width - abs( ( kx*x - kx*pulse_shift )/kx^2) ) "
#  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) * cos( pi/2 * ( x - pulse_shift - 2*pulse_width ) / pulse_width ) * step( pulse_width - abs( ( kx*x - kx*pulse_shift )/kx^2) ) "
# %

 # logistic pulse
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "        Ez * cos( kx * ( x - pulse_shift ) ) * ( 1 / ( 1 + exp( gr * ( x-pulse_shift-pulse_width/2 ) ) ) + 1 / ( 1 + exp( -gr * ( x-pulse_shift+pulse_width/2 ) ) ) - 1 ) " 
#  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) * ( 1 / ( 1 + exp( gr * ( x-pulse_shift-pulse_width/2 ) ) ) + 1 / ( 1 + exp( -gr * ( x-pulse_shift+pulse_width/2 ) ) ) - 1 ) "
# %

#ps = pulse_shift
#pw = pulse_width
 # trapezoidal pulse
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "        Ez*cos(kx*(x-ps)) * ((1-gr*(x-ps-pw/2)*step(x-ps-pw/2))*step(-(x-ps)+pw/2+1/gr) + (-1+gr*(x-ps+pw/2+1/gr)*step(x-ps+pw/2+1/gr))*step(-(x-ps)-pw/2)) " 
#  2 | formula | magnetic_field | " -1/c * Ez*cos(kx*(x-ps)) * ((1-gr*(x-ps-pw/2)*step(x-ps-pw/2))*step(-(x-ps)+pw/2+1/gr) + (-1+gr*(x-ps+pw/2+1/gr)*step(x-ps+pw/2+1/gr))*step(-(x-ps)-pw/2)) "
# %
# %UserDefinedInitialMaxwellStates
#  3 | formula | electric_field | "        Ez*cos(kx*(x-ps)) * ((-1+gr*(x-ps+pw/2+1/gr)*step(x-ps+pw/2+1/gr))*step(-(x-ps)-pw/2)) " 
#  2 | formula | magnetic_field | " -1/c * Ez*cos(kx*(x-ps)) * ((-1+gr*(x-ps+pw/2+1/gr)*step(x-ps+pw/2+1/gr))*step(-(x-ps)-pw/2)) "
# %


MaxwellIncidentWavesInBox           = no


# ----- spacial constant magnet field -----------

#By = 1.0

# %UserDefinedConstantSpacialMaxwellField
#   0 | 0 | 0 | 0 | By | 0 | "time_function"
# %

# %TDFunctions
#  "time_function" | tdf_logistic | 1.0 | 100.0 | 0.1 | 0.12
# %



# ----- external current ------------------------

# MaxwellExternalCurrent            = yes

# t0    = 20.0
# js    = 4.0
# jmax  = 1.000000000
# j_abs = 1.000000000

# %UserDefinedMaxwellExternalCurrent
#  external_current_td_function |  "0" | "0" | " jmax * step(x) * step(-x) * step(y) * step(-y) " | 0 | "external_current_envelope_function"
# %

# %TDFunctions
#  "external_current_envelope_function" | tdf_gaussian  | j_abs | js | t0
#  "external_current_phase"             | tdf_cw        | phi
# %


# ----- classical external field for td run -------------------------------------------------------

# lambda = 050.00
# omega  = 17.2205
# Ez     = 00.100000
# t0     = 16.05417558271833942943
# tau0   = -11.35201642089963093485
# phi    = - omega * t0

# %TDExternalFields
#   electric_field | 1 | 0 | 0 | omega | "envelope_gauss" | phase
# %

# %TDFunctions
#   "envelope_gauss" | tdf_gaussian  | 00.100000 | tau0 | t0
#   "phase"          | tdf_from_expr | "phi"
# %


# ----- spatial constant field propagation

# Ez          =  0.000010000000
# By          =  0.000010000000
# pulse_width =   500.0000
# pulse_shift =   270.0000
# pulse_slope =   100.0000

# ----- constant electric field

# %UserDefinedConstantSpacialMaxwellField
#   0 | 0 | Ez | 0 | 0 | 0 | "time_function"
# %

# %TDFunctions
#  "time_function" | tdf_logistic | 1.0 | pulse_slope | pulse_width | pulse_shift
# %

# ----- constant magnetic field
# %UserDefinedConstantSpacialMaxwellField
#   0 | 0 | 0 | 0 | By | 0 | "time_function"
# %

# %TDFunctions
#  "time_function" | tdf_logistic | 1.0 | pulse_slope | pulse_width | pulse_shift
# %


# ----- Medium Box ------------------------------

# % MaxwellMediumBox
#  0.0 | 0.0 | 0.0 | 10.0 | 10.0 | 10.0 | 2.0 | 2.0 | 0.0 | 0.0 | edged
# %

# MaxwellMediumCalculation = riemann_silberstein
