#! /bin/bash -l

module purge
module load intel impi mkl
module load gsl fftw-mpi hdf5-serial netcdf-serial
module load libxc
module load elpa
module list

export INSTALLDIR=$(pwd)


export MKL="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lmkl_blacs_intelmpi_lp64 -lpthread -lm -ldl"

export INSTALL_PREFIX=/mpcdf/soft/SLE_12_SP3/packages/skylake/
export METIS_HOME=$INSTALL_PREFIX/metis/intel_18_0_5/5.1.0
export PARMETIS_HOME=$INSTALL_PREFIX/parmetis/intel_18_0_5-impi_2018_4/4.0.3

export FCFLAGS_ELPA=$ELPA_FCFLAGS
export LIBS_ELPA=$ELPA_LIBS

./configure \
CC=mpiicc CFLAGS="-g -O3 -xCORE-AVX512 -qopt-zmm-usage=high -fma -ip -fzero-initialized-in-bss" \
CXX=mpiicpc CXXFLAGS="-g -O3 -xCORE-AVX512 -qopt-zmm-usage=high -fma -ip -fzero-initialized-in-bss" \
FC=mpiifort FCFLAGS="-g -O3 -xCORE-AVX512 -qopt-zmm-usage=high -fma -ip -fzero-initialized-in-bss -init=arrays -init=zero" \
LDFLAGS="-Xlinker -rpath=$MKL_HOME/lib/intel64:$GSL_HOME/lib:$NETCDF_HOME/lib:$ELPA_HOME/lib:$METIS_HOME/lib:$PARMETIS_HOME/lib:$FFTW_HOME/lib:$CUDA_HOME/lib64" \
FCCPP="cpp -ffreestanding" \
--prefix=$INSTALLDIR \
--enable-mpi --enable-openmp \
--disable-gdlib \
--enable-silent-rules \
--with-blas="$MKL" \
--with-lapack="$MKL" \
--with-blacs="$MKL" \
--with-scalapack="$MKL" \
--with-gsl-prefix="$GSL_HOME" \
--with-libxc-prefix="$LIBXC_HOME" \
--with-fftw-prefix="$FFTW_HOME" \
--with-netcdf-prefix="$NETCDF_HOME" \
--with-elpa-prefix="$ELPA_HOME" \
--with-metis-prefix="$METIS_HOME" || exit 1

# can be added if needed
#--with-parmetis-prefix="$PARMETIS_HOME" \

echo "\n\nBuilding octopus...\n"

make -j8 && make install

mkdir -p $INSTALLDIR/.build.doc/
cp -f config.log $INSTALLDIR/.build.doc/
cp -f $0 $INSTALLDIR/.build.doc/

echo "... done"
