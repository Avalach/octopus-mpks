!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: propagator_etrs.F90 15698 2016-10-28 12:49:03Z nicolastd $

#include "global.h"

module propagator_etrs_oct_m
  use accel_oct_m
  use batch_oct_m
  use batch_ops_oct_m
  use current_oct_m
  use density_oct_m
  use exponential_oct_m
  use epot_oct_m
  use gauge_field_oct_m
  use grid_oct_m
  use geometry_oct_m
  use global_oct_m
  use hamiltonian_oct_m
  use ion_dynamics_oct_m
  use lalg_basic_oct_m
  use loct_pointer_oct_m
  use math_oct_m
  use maxwell_hamiltonian_oct_m
  use maxwell_propagator_oct_m
  use messages_oct_m
  use mesh_function_oct_m
  use potential_interpolation_oct_m
  use profiling_oct_m
  use propagator_base_oct_m
  use states_dim_oct_m
  use states_oct_m
  use types_oct_m
  use v_ks_oct_m
  use xyz_adjust_oct_m

  implicit none

  private

  public ::                       &
    td_etrs,                      &
    td_etrs_sc,                   &
    td_aetrs

contains

  ! ---------------------------------------------------------
  !> Propagator with enforced time-reversal symmetry
  subroutine td_etrs(ks, hm, gr, st, tr, time, dt, ionic_scale, ions, geo, move_ions)
    type(v_ks_t), target,                 intent(inout) :: ks
    type(hamiltonian_t), target,          intent(inout) :: hm
    type(grid_t),        target,          intent(inout) :: gr
    type(states_t),      target,          intent(inout) :: st
    type(propagator_t),  target,          intent(inout) :: tr
    FLOAT,                                intent(in)    :: time
    FLOAT,                                intent(in)    :: dt
    FLOAT,                                intent(in)    :: ionic_scale
    type(ion_dynamics_t),                 intent(inout) :: ions
    type(geometry_t),                     intent(inout) :: geo
    logical,                              intent(in)    :: move_ions

    FLOAT, allocatable :: vhxc_t1(:,:), vhxc_t2(:,:)
    integer :: ik, ib
    type(batch_t) :: zpsib_dt
    type(density_calc_t) :: dens_calc

    PUSH_SUB(td_etrs)

    if(hm%theory_level /= INDEPENDENT_PARTICLES) then

      SAFE_ALLOCATE(vhxc_t1(1:gr%mesh%np, 1:st%d%nspin))
      SAFE_ALLOCATE(vhxc_t2(1:gr%mesh%np, 1:st%d%nspin))
      call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t1)

      call density_calc_init(dens_calc, st, gr, st%rho)

      do ik = st%d%kpt%start, st%d%kpt%end
        do ib = st%group%block_start, st%group%block_end

          call batch_copy(st%group%psib(ib, ik), zpsib_dt)
          if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(zpsib_dt, copy = .false.)

          !propagate the state dt/2 and dt, simultaneously, with H(time - dt)
          call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time - dt, &
            psib2 = zpsib_dt, deltat2 = dt)

          !use the dt propagation to calculate the density
          call density_calc_accumulate(dens_calc, ik, zpsib_dt)

          call batch_end(zpsib_dt)

        end do
      end do

      call density_calc_end(dens_calc)

      call v_ks_calc(ks, hm, st, geo, calc_current = gauge_field_is_applied(hm%ep%gfield))

      call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t2)
      call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t1, hm%vhxc)
      call hamiltonian_update(hm, gr%mesh, time = time - dt)

    else

      ! propagate dt/2 with H(time - dt)
      do ik = st%d%kpt%start, st%d%kpt%end
        do ib = st%group%block_start, st%group%block_end
          call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time - dt)
        end do
      end do

    end if

    ! propagate dt/2 with H(t)

    ! first move the ions to time t
    if(move_ions .and. ion_dynamics_ions_move(ions)) then
      call ion_dynamics_propagate(ions, gr%sb, geo, time, ionic_scale*dt)
      call hamiltonian_epot_generate(hm, gr, geo, st, time = time)
    end if

    if(gauge_field_is_applied(hm%ep%gfield)) then
      call gauge_field_propagate(hm%ep%gfield, dt, time)
    end if

    if(hm%theory_level /= INDEPENDENT_PARTICLES) then
      call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t2, hm%vhxc)
    end if

    call hamiltonian_update(hm, gr%mesh, time = time)

    do ik = st%d%kpt%start, st%d%kpt%end
      do ib = st%group%block_start, st%group%block_end
        call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time)
      end do
    end do

    if(hm%theory_level /= INDEPENDENT_PARTICLES) then
      SAFE_DEALLOCATE_A(vhxc_t1)
      SAFE_DEALLOCATE_A(vhxc_t2)
    end if

    if(.not. hm%cmplxscl%space) then
      call density_calc(st, gr, st%rho)
    else
      call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
    end if

    POP_SUB(td_etrs)
  end subroutine td_etrs

  ! ---------------------------------------------------------
  !> Propagator with enforced time-reversal symmetry and self-consistency
  subroutine td_etrs_sc(ks, hm, gr, st, tr, time, dt, ionic_scale, ions, geo, move_ions, sctol, scsteps, &
                        maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, maxwell_sctol)
    type(v_ks_t),               target,   intent(inout) :: ks
    type(hamiltonian_t),        target,   intent(inout) :: hm
    type(grid_t),               target,   intent(inout) :: gr
    type(states_t),             target,   intent(inout) :: st
    type(propagator_t),         target,   intent(inout) :: tr
    FLOAT,                                intent(in)    :: time
    FLOAT,                                intent(in)    :: dt
    FLOAT,                                intent(in)    :: ionic_scale
    type(ion_dynamics_t),                 intent(inout) :: ions
    type(geometry_t),                     intent(inout) :: geo
    logical,                              intent(in)    :: move_ions
    FLOAT,                                intent(in)    :: sctol
    integer,                    optional, intent(out)   :: scsteps
    type(hamiltonian_t),        optional, intent(inout) :: maxwell_hm
    type(grid_t),               optional, intent(inout) :: maxwell_gr
    type(states_t),             optional, intent(inout) :: maxwell_st
    type(maxwell_propagator_t), optional, intent(inout) :: maxwell_tr
    FLOAT,                      optional, intent(in)    :: maxwell_sctol

    FLOAT :: diff, maxwell_diff, mx_dt, mx_time
    FLOAT, allocatable :: vhxc_t1(:,:), vhxc_t2(:,:), maxwell_rs_state_diff(:), tmp_ma_gr(:,:), tmp_mx_gr(:,:)
    integer :: ik, ib, iter, ip, ig, mx_iter, mx_steps, idim
    type(batch_t) :: zpsib_dt
    type(density_calc_t) :: dens_calc
    type(batch_t), allocatable :: psi2(:, :)
    ! these are hardcoded for the moment
    integer, parameter :: niter = 10
    logical :: maxwell = .false. , pml_check = .false.
    CMPLX, allocatable :: maxwell_rs_current_density_t1(:,:), maxwell_rs_current_density_t2(:,:)
    CMPLX, allocatable :: maxwell_rs_current_density_ext(:,:)
    CMPLX, allocatable :: maxwell_rs_charge_density_t1(:), maxwell_rs_charge_density_t2(:)
    CMPLX, allocatable :: maxwell_rs_state_t1(:,:), maxwell_rs_state_t2(:,:), maxwell_rs_state_plane_waves(:,:)
    CMPLX              :: tmp(3)

    PUSH_SUB(td_etrs_sc)

    ASSERT(hm%theory_level /= INDEPENDENT_PARTICLES)

    maxwell = present(maxwell_hm) .and. present(maxwell_gr) .and. present(maxwell_st) .and. present(maxwell_tr)

    ! Maxwell field propagation: auxiliary arrays and RS rs_density at time t1 (1 of 3 "if (maxwell)" insertions)
    if(maxwell) then

      do idim=1, 3
        if (maxwell_hm%maxwell_bc%bc_ab_type(idim) == OPTION__MAXWELLABSORBINGBOUNDARIES__CPML) then
          pml_check = .true.
        end if
      end do

      ! auxiliary arrays initialization
      SAFE_ALLOCATE(maxwell_rs_state_t1(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      SAFE_ALLOCATE(maxwell_rs_state_t2(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      SAFE_ALLOCATE(maxwell_rs_state_diff(1:maxwell_gr%mesh%np))
      if (maxwell_hm%ma_mx_coupling_apply .or. maxwell_hm%maxwell_current_density_ext_flag) then
        SAFE_ALLOCATE(maxwell_rs_current_density_t1(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
        maxwell_rs_current_density_t1  = M_z0
        SAFE_ALLOCATE(maxwell_rs_current_density_t2(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
        maxwell_rs_current_density_t2  = M_z0
        if (maxwell_hm%maxwell_current_density_ext_flag) then
          SAFE_ALLOCATE(maxwell_rs_current_density_ext(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
          maxwell_rs_current_density_ext = M_z0
        end if
        if (maxwell_hm%maxwell_operator == OPTION__MAXWELLHAMILTONIANOPERATOR__FARADAY_AMPERE_GAUSS) then
          SAFE_ALLOCATE(maxwell_rs_charge_density_t1(1:maxwell_gr%mesh%np_part))
          maxwell_rs_charge_density_t1 = M_z0
          SAFE_ALLOCATE(maxwell_rs_charge_density_t2(1:maxwell_gr%mesh%np_part))
          maxwell_rs_charge_density_t2 = M_z0
        end if
      end if
      SAFE_ALLOCATE(maxwell_rs_state_plane_waves(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))

      ! storage of the RS state of the old step time t-dt
      maxwell_rs_state_t1(:,:) = maxwell_st%maxwell_rs_state(:,:)

      ! storage or the RS incident waves state without any coupling
      if (maxwell_tr%maxwell_bc_plane_waves) then
        maxwell_rs_state_plane_waves(:,:) = maxwell_st%maxwell_rs_state_plane_waves(:,:)
      end if

      if (maxwell_hm%maxwell_propagation_apply) then
        if (maxwell_hm%ma_mx_coupling_apply) then
          ! calculation of rs_density at time (time-dt)
          call maxwell_get_rs_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, hm%mx_ma_coupling_points, &
                                      hm%mx_ma_coupling_points_number, maxwell_rs_current_density_t1,    &
                                      maxwell_rs_charge_density_t1, time-dt, tr%current_prop_test)
        end if
        ! calculation of the external RS density at time (time-dt) 
        if (maxwell_hm%maxwell_current_density_ext_flag) then
          call maxwell_get_rs_density_ext(maxwell_st, maxwell_gr%mesh, time-dt, maxwell_rs_current_density_ext)
          if (.not. maxwell_hm%ma_mx_coupling_apply) maxwell_rs_current_density_t1 = M_z0
          maxwell_rs_current_density_t1 = maxwell_rs_current_density_t1 + maxwell_rs_current_density_ext
        end if
        ! store old convolution function of cpml calculation
        if (pml_check) then
          maxwell_hm%maxwell_bc%pml_conv_plus_old  = maxwell_hm%maxwell_bc%pml_conv_plus
          maxwell_hm%maxwell_bc%pml_conv_minus_old = maxwell_hm%maxwell_bc%pml_conv_minus
        end if
        ! calculation of the Maxwell to matter coupling points at time (time)
        call maxwell_get_mx_ma_coupling_points(geo, hm, hm%mx_ma_coupling_points(:,:))
        ! get the map of each grid point to the next Maxwell to matter coupling point
        call maxwell_grid_points_coupling_points_mapping(gr, hm%mx_ma_coupling_points, hm%mx_ma_coupling_points_number, &
                                                         hm%mx_ma_coupling_points_map)
      end if

      call hamiltonian_update(hm, gr%mesh, time = time - dt)

    end if

    SAFE_ALLOCATE(vhxc_t1(1:gr%mesh%np, 1:st%d%nspin))
    SAFE_ALLOCATE(vhxc_t2(1:gr%mesh%np, 1:st%d%nspin))
    call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t1)

    call messages_new_line()
    call messages_write('        Self-consistency iteration:')
    call messages_info()

    call density_calc_init(dens_calc, st, gr, st%rho)

    do ik = st%d%kpt%start, st%d%kpt%end
      do ib = st%group%block_start, st%group%block_end

        call batch_copy(st%group%psib(ib, ik), zpsib_dt)
        if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(zpsib_dt, copy = .false.)

        !propagate the state dt/2 and dt, simultaneously, with H(time - dt)
        call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time - dt, &
          psib2 = zpsib_dt, deltat2 = dt)

        !use the dt propagation to calculate the density
        call density_calc_accumulate(dens_calc, ik, zpsib_dt)

        call batch_end(zpsib_dt)

      end do
    end do

    call density_calc_end(dens_calc)

    call v_ks_calc(ks, hm, st, geo, calc_current = gauge_field_is_applied(hm%ep%gfield))

    call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t2)
    call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t1, hm%vhxc)
    call hamiltonian_update(hm, gr%mesh, time = time - dt)

    ! propagate dt/2 with H(t)

    ! first move the ions to time t
    if(move_ions .and. ion_dynamics_ions_move(ions)) then
      call ion_dynamics_propagate(ions, gr%sb, geo, time, ionic_scale*dt)
      call hamiltonian_epot_generate(hm, gr, geo, st, time = time)
    end if

    if(gauge_field_is_applied(hm%ep%gfield)) then
      call gauge_field_propagate(hm%ep%gfield, dt, time)
    end if

    if(hm%theory_level /= INDEPENDENT_PARTICLES) then
      call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t2, hm%vhxc)
    end if

    call hamiltonian_update(hm, gr%mesh, time = time)

    ! Maxwell field propagation: first prediction of Maxell field propagation (2 of 3 "if (maxwell)" insertions)
    if (maxwell) then

      if (maxwell_hm%maxwell_propagation_apply) then
        if (maxwell_hm%ma_mx_coupling_apply) then
          ! calculation of rs_density at time (time)
          call maxwell_get_rs_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, hm%mx_ma_coupling_points, &
                                      hm%mx_ma_coupling_points_number, maxwell_rs_current_density_t2,    &
                                      maxwell_rs_charge_density_t2, time, tr%current_prop_test)
        end if
        ! calculation of external RS density at time (time)
        if (maxwell_hm%maxwell_current_density_ext_flag) then
          call maxwell_get_rs_density_ext(maxwell_st, maxwell_gr%mesh, time, maxwell_rs_current_density_ext)
          if (.not. maxwell_hm%ma_mx_coupling_apply) maxwell_rs_current_density_t2 = M_z0
          maxwell_rs_current_density_t2 = maxwell_rs_current_density_t2 + maxwell_rs_current_density_ext
        end if
        ! reset old convolution function of cpml calculation
        if (pml_check) then
          maxwell_hm%maxwell_bc%pml_conv_plus  = maxwell_hm%maxwell_bc%pml_conv_plus_old
          maxwell_hm%maxwell_bc%pml_conv_minus = maxwell_hm%maxwell_bc%pml_conv_minus_old
        end if
        ! Setting electron density for Maxwell grid
        if (maxwell_hm%maxwell_diamag_current) then
          SAFE_ALLOCATE(tmp_ma_gr(1:gr%mesh%np_part,1:st%d%nspin))
          SAFE_ALLOCATE(tmp_mx_gr(1:maxwell_gr%mesh%np_part,1:st%d%nspin))
          tmp_ma_gr = M_ZERO
          tmp_ma_gr(1:gr%mesh%np,1:st%d%nspin) = st%rho(1:gr%mesh%np,1:st%d%nspin)
          tmp_mx_gr = M_ZERO
          call dmaxwell_ma_mesh_to_mx_mesh(maxwell_st, maxwell_gr, st, gr, tmp_ma_gr, tmp_mx_gr, st%d%nspin)
          maxwell_st%maxwell_grid_rho(1:gr%mesh%np,1:st%d%nspin) = tmp_mx_gr(1:gr%mesh%np,1:st%d%nspin)
        end if
        ! Maxwell propagation step from time (time-dt) to (time) as a first prediction
        call maxwell_propagation_etrs(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, maxwell_st%maxwell_rs_state, & 
                                      maxwell_rs_current_density_t1, maxwell_rs_current_density_t2,                &
                                      maxwell_rs_charge_density_t1, maxwell_rs_charge_density_t2, time-dt, dt,     &
                                      maxwell_rs_state_t1)
        ! calculation of the Maxwell to matter coupling points at time (time)
        call maxwell_get_mx_ma_coupling_points(geo, hm, hm%mx_ma_coupling_points(:,:))
        ! get the map of each grid point to the next Maxwell to matter coupling point
        call maxwell_grid_points_coupling_points_mapping(gr, hm%mx_ma_coupling_points, hm%mx_ma_coupling_points_number, &
                                                         hm%mx_ma_coupling_points_map)
      end if

      ! coupling of Maxwell fields to the matter
      if (hm%mx_ma_coupling_apply .and. (tr%current_prop_test == 0)) then
        ! transverse field calculation
        call maxwell_get_vector_pot_and_transverse_field(hm%mx_ma_trans_field_calc_method, maxwell_gr, maxwell_hm, maxwell_st, &
                                                         maxwell_tr, gr, hm, st, tr, maxwell_hm%maxwell_poisson_solver, time,  &
                                                         maxwell_st%maxwell_rs_state, maxwell_st%maxwell_rs_state_trans,       &
                                                         maxwell_hm%maxwell_vector_potential)
        ! electric potential calculation
        call epot_generate_maxwell_coupling(hm%ep, gr, maxwell_st, maxwell_gr)
      else
        maxwell_st%maxwell_rs_state_trans = maxwell_st%maxwell_rs_state
      end if
      ! Storage of the first predicted Maxwell RS state for sc step comparsion
      maxwell_rs_state_t2(:,:) = maxwell_st%maxwell_rs_state(:,:)

    end if

    SAFE_ALLOCATE(psi2(st%group%block_start:st%group%block_end, st%d%kpt%start:st%d%kpt%end))

    ! store the state at half iteration
    do ik = st%d%kpt%start, st%d%kpt%end
      do ib = st%group%block_start, st%group%block_end
        call batch_copy(st%group%psib(ib, ik), psi2(ib, ik))
        if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(psi2(ib, ik), copy = .false.)
        call batch_copy_data(gr%mesh%np, st%group%psib(ib, ik), psi2(ib, ik))
      end do
    end do

    do iter = 1, niter

      call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t2)

      do ik = st%d%kpt%start, st%d%kpt%end
        do ib = st%group%block_start, st%group%block_end
          call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time)
        end do
      end do

      if(.not. hm%cmplxscl%space) then
        call density_calc(st, gr, st%rho)
      else
        call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
      end if

      call v_ks_calc(ks, hm, st, geo, time = time, calc_current = gauge_field_is_applied(hm%ep%gfield))

      ! Maxwell field propagation: self consistency step of Maxell field propagation (3 of 3 "if (maxwell)" insertions)
      if(maxwell) then

        ! reset RS state to the old correct step time t-dt
        maxwell_st%maxwell_rs_state(:,:) = maxwell_rs_state_t1(:,:)

        ! reset RS incident waves state without any coupling
        if (maxwell_tr%maxwell_bc_plane_waves) then
          maxwell_st%maxwell_rs_state_plane_waves(:,:) = maxwell_rs_state_plane_waves(:,:)
        end if

        if (maxwell_hm%maxwell_propagation_apply) then
          if (maxwell_hm%ma_mx_coupling_apply) then
            ! calculation of rs_density at time (time)
            call maxwell_get_rs_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, hm%mx_ma_coupling_points, &
                                        hm%mx_ma_coupling_points_number, maxwell_rs_current_density_t2,    &
                                        maxwell_rs_charge_density_t2, time, tr%current_prop_test)
          end if
          ! calculation of external RS density at time (time)
          if (maxwell_hm%maxwell_current_density_ext_flag) then
            call maxwell_get_rs_density_ext(maxwell_st, maxwell_gr%mesh, time, maxwell_rs_current_density_ext)
            if (.not. maxwell_hm%ma_mx_coupling_apply) maxwell_rs_current_density_t2 = M_z0
            maxwell_rs_current_density_t2 = maxwell_rs_current_density_t2 + maxwell_rs_current_density_ext
          end if
          ! reset old convolution function of cpml calculation
          if (pml_check) then
          maxwell_hm%maxwell_bc%pml_conv_plus  = maxwell_hm%maxwell_bc%pml_conv_plus_old
          maxwell_hm%maxwell_bc%pml_conv_minus = maxwell_hm%maxwell_bc%pml_conv_minus_old
          end if
          ! Setting electron density for Maxwell grid
          if (maxwell_hm%maxwell_diamag_current) then
            tmp_ma_gr = M_ZERO
            tmp_ma_gr(1:gr%mesh%np,1:st%d%nspin) = st%rho(1:gr%mesh%np,1:st%d%nspin)
            tmp_mx_gr = M_ZERO
            call dmaxwell_ma_mesh_to_mx_mesh(maxwell_st, maxwell_gr, st, gr, tmp_ma_gr, tmp_mx_gr, st%d%nspin)
            maxwell_st%maxwell_grid_rho(1:gr%mesh%np,1:st%d%nspin) = tmp_mx_gr(1:gr%mesh%np,1:st%d%nspin)
            SAFE_DEALLOCATE_A(tmp_ma_gr)
            SAFE_DEALLOCATE_A(tmp_mx_gr)
          end if
          ! Maxwell propagation step from time (time-dt) to (time) as a first prediction
          call maxwell_propagation_etrs(maxwell_hm, maxwell_gr, maxwell_st, maxwell_tr, maxwell_st%maxwell_rs_state, & 
                                        maxwell_rs_current_density_t1, maxwell_rs_current_density_t2,                &
                                        maxwell_rs_charge_density_t1, maxwell_rs_charge_density_t2, time-dt, dt,     &
                                        maxwell_rs_state_t1)
        end if

        ! calculation of the Maxwell to matter coupling points at time (time)
        call maxwell_get_mx_ma_coupling_points(geo, hm, hm%mx_ma_coupling_points(:,:))
        ! get the map of each grid point to the next Maxwell to matter coupling point
        call maxwell_grid_points_coupling_points_mapping(gr, hm%mx_ma_coupling_points, hm%mx_ma_coupling_points_number, &
                                                         hm%mx_ma_coupling_points_map)

        ! coupling of Maxwell fields to the matter
        if (hm%mx_ma_coupling_apply .and. (tr%current_prop_test == 0)) then
          ! transverse field calculation
          call maxwell_get_vector_pot_and_transverse_field(hm%mx_ma_trans_field_calc_method, maxwell_gr, maxwell_hm, maxwell_st, &
                                                           maxwell_tr, gr, hm, st, tr, maxwell_hm%maxwell_poisson_solver, time,  &
                                                           maxwell_st%maxwell_rs_state, maxwell_st%maxwell_rs_state_trans,       &
                                                           maxwell_hm%maxwell_vector_potential)
          ! electric potential calculation
          call epot_generate_maxwell_coupling(hm%ep, gr, maxwell_st, maxwell_gr)
        else
          maxwell_st%maxwell_rs_state_trans = maxwell_st%maxwell_rs_state
        end if

      end if

      ! now check how much the potential changed
      do ip = 1, gr%mesh%np
        vhxc_t2(ip, 1) = sum(abs(vhxc_t2(ip, 1:st%d%nspin) - hm%vhxc(ip, 1:st%d%nspin)))
      end do
      diff = dmf_integrate(gr%mesh, vhxc_t2(:, 1))

      ! now check how much the electromagnetic field changed
      if (maxwell) then
        do ip=1, maxwell_gr%mesh%np
          maxwell_rs_state_diff(ip) = sum(abs(maxwell_rs_state_t2(ip,1:maxwell_st%d%dim) & 
                                             -maxwell_st%maxwell_rs_state(ip,1:maxwell_st%d%dim)))
        end do
        maxwell_diff = dmf_integrate(maxwell_gr%mesh, maxwell_rs_state_diff(:))
      end if

      if (maxwell) then
        call messages_write('          step ')
        call messages_write(iter)
        call messages_write(', KS-residue = ')
        call messages_write(abs(diff), fmt = '(1x,es9.2)')
        call messages_write(', Maxwell-residue = ')
        call messages_write(abs(maxwell_diff), fmt = '(1x,es9.2)')
        call messages_info()
      else
        call messages_write('          step ')
        call messages_write(iter)
        call messages_write(', residue = ')
        call messages_write(abs(diff), fmt = '(1x,es9.2)')
        call messages_info()
      end if

      if(maxwell) then
        if((diff <= sctol) .and. (maxwell_diff <= maxwell_sctol)) exit
      else
        if(diff <= sctol) exit
      end if

      if(iter /= niter) then
        ! we are not converged, restore the states
        do ik = st%d%kpt%start, st%d%kpt%end
          do ib = st%group%block_start, st%group%block_end
            call batch_copy_data(gr%mesh%np, psi2(ib, ik), st%group%psib(ib, ik))
          end do
        end do
        ! copy current RS state for next sc step correction
        if (maxwell) maxwell_rs_state_t2(:,:) = maxwell_st%maxwell_rs_state(:,:)
      end if

    end do

    ! print an empty line
    call messages_info()

    if(present(scsteps)) scsteps = iter

    SAFE_DEALLOCATE_A(vhxc_t1)
    SAFE_DEALLOCATE_A(vhxc_t2)

    if (maxwell) then
      ! auxiliary arrays deallocation
      SAFE_DEALLOCATE_A(maxwell_rs_state_t1)
      SAFE_DEALLOCATE_A(maxwell_rs_state_t2)
      SAFE_DEALLOCATE_A(maxwell_rs_state_diff)
      if (maxwell_hm%ma_mx_coupling_apply .or. maxwell_hm%maxwell_current_density_ext_flag) then
        SAFE_DEALLOCATE_A(maxwell_rs_current_density_t1)
        SAFE_DEALLOCATE_A(maxwell_rs_current_density_t2)
        if (maxwell_hm%maxwell_current_density_ext_flag) then
          SAFE_DEALLOCATE_A(maxwell_rs_current_density_ext)
        end if
        if (maxwell_hm%maxwell_operator == OPTION__MAXWELLHAMILTONIANOPERATOR__FARADAY_AMPERE_GAUSS) then
          SAFE_DEALLOCATE_A(maxwell_rs_charge_density_t1)
          SAFE_DEALLOCATE_A(maxwell_rs_charge_density_t2)
        end if
      end if
      SAFE_DEALLOCATE_A(maxwell_rs_state_plane_waves)
    end if

    do ik = st%d%kpt%start, st%d%kpt%end
      do ib = st%group%block_start, st%group%block_end
        call batch_end(psi2(ib, ik))
      end do
    end do

    SAFE_DEALLOCATE_A(psi2)

    POP_SUB(td_etrs_sc)
  end subroutine td_etrs_sc

  ! ---------------------------------------------------------
  !> Propagator with approximate enforced time-reversal symmetry
  subroutine td_aetrs(ks, hm, gr, st, tr, time, dt, ionic_scale, ions, geo, move_ions)
    type(v_ks_t), target,            intent(inout) :: ks
    type(hamiltonian_t), target,     intent(inout) :: hm
    type(grid_t),        target,     intent(inout) :: gr
    type(states_t),      target,     intent(inout) :: st
    type(propagator_t),  target,     intent(inout) :: tr
    FLOAT,                           intent(in)    :: time
    FLOAT,                           intent(in)    :: dt
    FLOAT,                           intent(in)    :: ionic_scale
    type(ion_dynamics_t),            intent(inout) :: ions
    type(geometry_t),                intent(inout) :: geo
    logical,                         intent(in)    :: move_ions

    integer :: ik, ispin, ip, ist, ib
    FLOAT :: vv
    CMPLX :: phase
    type(density_calc_t)  :: dens_calc
    type(profile_t), save :: phase_prof
    integer               :: pnp, iprange
    FLOAT, allocatable    :: vold(:, :), imvold(:, :)
    type(accel_mem_t)    :: phase_buff

    PUSH_SUB(td_aetrs)

    if(tr%method == PROP_CAETRS) then
      SAFE_ALLOCATE(vold(1:gr%mesh%np, 1:st%d%nspin))
      if(hm%cmplxscl%space) then
        SAFE_ALLOCATE(Imvold(1:gr%mesh%np, 1:st%d%nspin))
        call potential_interpolation_get(tr%vksold, gr%mesh%np, st%d%nspin, 2, vold, imvold)
      else
        call potential_interpolation_get(tr%vksold, gr%mesh%np, st%d%nspin, 2, vold)
      end if
      call lalg_copy(gr%mesh%np, st%d%nspin, vold, hm%vhxc)
      if(hm%cmplxscl%space) call lalg_copy(gr%mesh%np, st%d%nspin, Imvold, hm%Imvhxc)
      call hamiltonian_update(hm, gr%mesh, time = time - dt)
      call v_ks_calc_start(ks, hm, st, geo, time = time - dt, calc_energy = .false., &
             calc_current = gauge_field_is_applied(hm%ep%gfield))
    end if

    ! propagate half of the time step with H(time - dt)
    do ik = st%d%kpt%start, st%d%kpt%end
      do ib = st%group%block_start, st%group%block_end
        call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time - dt)
      end do
    end do

    if(tr%method == PROP_CAETRS) then
      call v_ks_calc_finish(ks, hm)

      call potential_interpolation_set(tr%vksold, gr%mesh%np, st%d%nspin, 1, hm%vhxc)
      call interpolate( (/time - dt, time - M_TWO*dt, time - M_THREE*dt/), &
        tr%vksold%v_old(:, :, 1:3), time, tr%vksold%v_old(:, :, 0))

      forall(ispin = 1:st%d%nspin, ip = 1:gr%mesh%np) 
        vold(ip, ispin) =  CNST(0.5)*dt*(hm%vhxc(ip, ispin) - vold(ip, ispin))
      end forall

      ! copy vold to a cl buffer
      if(accel_is_enabled() .and. hamiltonian_apply_packed(hm, gr%mesh)) then
        pnp = accel_padded_size(gr%mesh%np)
        call accel_create_buffer(phase_buff, ACCEL_MEM_READ_ONLY, TYPE_FLOAT, pnp*st%d%nspin)
        ASSERT(ubound(vold, dim = 1) == gr%mesh%np)
        do ispin = 1, st%d%nspin
          call accel_write_buffer(phase_buff, gr%mesh%np, vold(:, ispin), offset = (ispin - 1)*pnp)
        end do
      end if

    end if

    call potential_interpolation_get(tr%vksold, gr%mesh%np, st%d%nspin, 0, hm%vhxc)

    ! move the ions to time t
    if(move_ions .and. ion_dynamics_ions_move(ions)) then
      call ion_dynamics_propagate(ions, gr%sb, geo, time, ionic_scale*dt)
      call hamiltonian_epot_generate(hm, gr, geo, st, time = time)
    end if

    if(gauge_field_is_applied(hm%ep%gfield)) then
      call gauge_field_propagate(hm%ep%gfield, dt, time)
    end if

    call hamiltonian_update(hm, gr%mesh, time = time)

    call density_calc_init(dens_calc, st, gr, st%rho)

    ! propagate the other half with H(t)
    do ik = st%d%kpt%start, st%d%kpt%end
      ispin = states_dim_get_spin_index(st%d, ik)

      do ib = st%group%block_start, st%group%block_end
        if(hamiltonian_apply_packed(hm, gr%mesh)) call batch_pack(st%group%psib(ib, ik))

        if(tr%method == PROP_CAETRS) then
          call profiling_in(phase_prof, "CAETRS_PHASE")
          select case(batch_status(st%group%psib(ib, ik)))
          case(BATCH_NOT_PACKED)
            do ip = 1, gr%mesh%np
              vv = vold(ip, ispin)
              phase = TOCMPLX(cos(vv), -sin(vv))
              forall(ist = 1:st%group%psib(ib, ik)%nst_linear)
                st%group%psib(ib, ik)%states_linear(ist)%zpsi(ip) = st%group%psib(ib, ik)%states_linear(ist)%zpsi(ip)*phase
              end forall
            end do
          case(BATCH_PACKED)
            do ip = 1, gr%mesh%np
              vv = vold(ip, ispin)
              phase = TOCMPLX(cos(vv), -sin(vv))
              forall(ist = 1:st%group%psib(ib, ik)%nst_linear)
                st%group%psib(ib, ik)%pack%zpsi(ist, ip) = st%group%psib(ib, ik)%pack%zpsi(ist, ip)*phase
              end forall
            end do
          case(BATCH_CL_PACKED)
            call accel_set_kernel_arg(kernel_phase, 0, pnp*(ispin - 1))
            call accel_set_kernel_arg(kernel_phase, 1, phase_buff)
            call accel_set_kernel_arg(kernel_phase, 2, st%group%psib(ib, ik)%pack%buffer)
            call accel_set_kernel_arg(kernel_phase, 3, log2(st%group%psib(ib, ik)%pack%size(1)))

            iprange = accel_max_workgroup_size()/st%group%psib(ib, ik)%pack%size(1)

            call accel_kernel_run(kernel_phase, (/st%group%psib(ib, ik)%pack%size(1), pnp/), &
              (/st%group%psib(ib, ik)%pack%size(1), iprange/))
          end select
          call profiling_out(phase_prof)
        end if

        call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, CNST(0.5)*dt, time)
        call density_calc_accumulate(dens_calc, ik, st%group%psib(ib, ik))

        if(hamiltonian_apply_packed(hm, gr%mesh)) call batch_unpack(st%group%psib(ib, ik))
      end do
    end do

    if(tr%method == PROP_CAETRS .and. accel_is_enabled() .and. hamiltonian_apply_packed(hm, gr%mesh)) then
      call accel_release_buffer(phase_buff)
    end if

    call density_calc_end(dens_calc)

    if(tr%method == PROP_CAETRS) then
      SAFE_DEALLOCATE_A(vold)
    end if

    POP_SUB(td_aetrs)
  end subroutine td_aetrs

end module propagator_etrs_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
