# -*- coding: utf-8 mode: shell-script -*-
# $Id: 01-propagators.test 14672 2015-10-17 18:16:07Z xavier $

Test       : Free Maxwell propagation
Program    : octopus
TestGroups : short-run, maxwell
Enabled    : Yes


# Large spacing
Processors : 4
Input      : 01-maxwell_free.01-laser_propagation.inp
Precision  : 1.0e-6

match ; Tot. Maxwell energy [step  0] ; LINEFIELD(td.general/maxwell_energy, -11, 3) ; 7.746672240841e+01
match ; Tot. Maxwell energy [step  5] ; LINEFIELD(td.general/maxwell_energy,  -6, 3) ; 5.195085837657e+01
match ; Tot. Maxwell energy [step 10] ; LINEFIELD(td.general/maxwell_energy,  -1, 3) ; 4.677759729376e+01

Precision  : 1.0e-12
match ; Ex (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-x\.y=0\,z=0, -45, 2) ; 0.00000000000000E+000
match ; Ex (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-x\.y=0\,z=0, -45, 2) ; 4.01254049997114E-018
match ; Ex (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-x\.y=0\,z=0, -45, 2) ; 1.29016889697387E-018
match ; Ex (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-x\.x=0\,z=0,  -5, 2) ; 0.00000000000000E+000
match ; Ex (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-x\.x=0\,z=0,  -5, 2) ; 6.02030788811250E-021
match ; Ex (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-x\.x=0\,z=0,  -5, 2) ; -2.76789543136491E-018
match ; Ex (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-x\.x=0\,y=0, -45, 2) ; 0.00000000000000E+000
match ; Ex (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-x\.x=0\,y=0, -45, 2) ; -8.42135363462883E-005
match ; Ex (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-x\.x=0\,y=0, -45, 2) ; 9.27504650857853E-004
match ; Ey (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-y\.y=0\,z=0, -45, 2) ; 0.00000000000000E+000
match ; Ey (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-y\.y=0\,z=0, -45, 2) ; 2.37571889051116E-019
match ; Ey (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-y\.y=0\,z=0, -45, 2) ; -1.73955772977982E-019
match ; Ey (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-y\.x=0\,z=0,  -5, 2) ; 0.00000000000000E+000
match ; Ey (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-y\.x=0\,z=0,  -5, 2) ; -1.94105342905003E-020
match ; Ey (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-y\.x=0\,z=0,  -5, 2) ; -4.53356051055379E-019
match ; Ey (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-y\.x=0\,y=0, -45, 2) ; 0.00000000000000E+000
match ; Ey (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-y\.x=0\,y=0, -45, 2) ; 3.89095123100155E-020
match ; Ey (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-y\.x=0\,y=0, -45, 2) ; -8.13101622539000E-019

Precision  : 1.0e-8
match ; Ez (x=  0,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.x=0\,y=0, -25, 2) ; 1.83157514239089E-002
match ; Ez (x=  0,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.x=0\,y=0, -25, 2) ; 2.24099298562919E-002
match ; Ez (x=  0,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.x=0\,y=0, -25, 2) ; -9.41721841255259E-002
match ; Ez (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.y=0\,z=0, -45, 2) ; 1.00000000000000E+000
match ; Ez (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.y=0\,z=0, -45, 2) ; -1.04406369421049E-001
match ; Ez (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.y=0\,z=0, -45, 2) ; -2.69768767000054E-003
match ; Ez (x= 10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.y=0\,z=0,  -5, 2) ; 1.60381089054861E-028
match ; Ez (x= 10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.y=0\,z=0,  -5, 2) ; 1.88567348104604E-003
match ; Ez (x= 10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.y=0\,z=0,  -5, 2) ; -3.26075024848850E-004
match ; Ez (x=  0,y=-10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.x=0\,z=0, -45, 2) ; 1.12535174719259E-007
match ; Ez (x=  0,y=-10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.x=0\,z=0, -45, 2) ; -1.00526552982403E-003
match ; Ez (x=  0,y=-10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.x=0\,z=0, -45, 2) ; 4.16638629161359E-002
match ; Ez (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.x=0\,z=0,  -5, 2) ; 1.12535174719259E-007
match ; Ez (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.x=0\,z=0,  -5, 2) ; -1.00526552982403E-003
match ; Ez (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.x=0\,z=0,  -5, 2) ; 4.16638629161359E-002
match ; Ez (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.x=0\,y=0, -45, 2) ; 1.12535174719259E-007
match ; Ez (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.x=0\,y=0, -45, 2) ; -1.01761278163445E-003
match ; Ez (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.x=0\,y=0, -45, 2) ; 4.20277783466986E-002
match ; Ez (x=  0,y=  0,z= 10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_e_field-z\.x=0\,y=0,  -5, 2) ; 1.12535174719259E-007
match ; Ez (x=  0,y=  0,z= 10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_e_field-z\.x=0\,y=0,  -5, 2) ; -1.01761278163445E-003
match ; Ez (x=  0,y=  0,z= 10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_e_field-z\.x=0\,y=0,  -5, 2) ; 4.20277783466986E-002

Precision  : 1.0e-12
match ; Bx (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-x\.y=0\,z=0, -45, 2) ; 0.00000000000000E+000
match ; Bx (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-x\.y=0\,z=0, -45, 2) ; -3.78579252171525E-020
match ; Bx (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-x\.y=0\,z=0, -45, 2) ; -7.69221186982931E-022
match ; Bx (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-x\.x=0\,z=0,  -5, 2) ; 0.00000000000000E+000
match ; Bx (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-x\.x=0\,z=0,  -5, 2) ; -1.32767156266733E-007
match ; Bx (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-x\.x=0\,z=0,  -5, 2) ; 6.74753383017239E-006
match ; Bx (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-x\.x=0\,y=0, -45, 2) ; 0.00000000000000E+000
match ; Bx (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-x\.x=0\,y=0, -45, 2) ; -3.13298744860596E-022
match ; Bx (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-x\.x=0\,y=0, -45, 2) ; 9.96323137880218E-021

Precision  : 1.0e-8
match ; By (x=  0,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.x=0\,y=0, -25, 2) ; -1.33656495131299E-004
match ; By (x=  0,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.x=0\,y=0, -25, 2) ; -3.04451232083558E-004
match ; By (x=  0,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.x=0\,y=0, -25, 2) ; 7.35577608430687E-004
match ; By (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.y=0\,z=0, -45, 2) ; -7.29735256635498E-003
match ; By (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.y=0\,z=0, -45, 2) ; 7.61890540609548E-004
match ; By (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.y=0\,z=0, -45, 2) ; 1.97108622459081E-005
match ; By (x= 10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.y=0\,z=0,  -5, 2) ; -1.17035735180929E-030
match ; By (x= 10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.y=0\,z=0,  -5, 2) ; -1.39123296006239E-005
match ; By (x= 10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.y=0\,z=0,  -5, 2) ; 1.55370589166820E-006
match ; By (x=  0,y=-10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.x=0\,z=0, -45, 2) ; -8.21208846042787E-010
match ; By (x=  0,y=-10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.x=0\,z=0, -45, 2) ; 7.39600334092347E-006
match ; By (x=  0,y=-10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.x=0\,z=0, -45, 2) ; -3.07178880173758E-004
match ; By (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.x=0\,z=0,  -5, 2) ; -8.21208846042787E-010
match ; By (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.x=0\,z=0,  -5, 2) ; 7.39600334092348E-006
match ; By (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.x=0\,z=0,  -5, 2) ; -3.07178880173758E-004
match ; By (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.x=0\,y=0, -45, 2) ; -8.21208846042787E-010
match ; By (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.x=0\,y=0, -45, 2) ; 6.65390149620714E-006
match ; By (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.x=0\,y=0, -45, 2) ; -3.04191635425628E-004
match ; By (x=  0,y=  0,z= 10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-y\.x=0\,y=0,  -5, 2) ; -8.21208846042787E-010
match ; By (x=  0,y=  0,z= 10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-y\.x=0\,y=0,  -5, 2) ; 6.65390149620716E-006
match ; By (x=  0,y=  0,z= 10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-y\.x=0\,y=0,  -5, 2) ; -3.04191635425628E-004

Precision  : 1.0e-12
match ; Bz (x=-10,y=  0,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-z\.y=0\,z=0, -45, 2) ; 0.00000000000000E+000
match ; Bz (x=-10,y=  0,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-z\.y=0\,z=0, -45, 2) ; 1.00235882303347E-021
match ; Bz (x=-10,y=  0,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-z\.y=0\,z=0, -45, 2) ; -1.19062190571189E-021
match ; Bz (x=  0,y= 10,z=  0) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-z\.x=0\,z=0,  -5, 2) ; 0.00000000000000E+000
match ; Bz (x=  0,y= 10,z=  0) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-z\.x=0\,z=0,  -5, 2) ; 7.17011268740656E-023
match ; Bz (x=  0,y= 10,z=  0) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-z\.x=0\,z=0,  -5, 2) ; -5.93227952878414E-021
match ; Bz (x=  0,y=  0,z=-10) [step  0] ; LINEFIELD(output_iter/td.0000000/maxwell_b_field-z\.x=0\,y=0, -45, 2) ; 0.00000000000000E+000
match ; Bz (x=  0,y=  0,z=-10) [step  5] ; LINEFIELD(output_iter/td.0000005/maxwell_b_field-z\.x=0\,y=0, -45, 2) ; -2.24969982677535E-022
match ; Bz (x=  0,y=  0,z=-10) [step 10] ; LINEFIELD(output_iter/td.0000010/maxwell_b_field-z\.x=0\,y=0, -45, 2) ; 1.86887415417218E-021

