#! /bin/bash -l

module purge
module load impi
module load intel mkl
module load gcc
module load gsl fftw netcdf-serial
module load libxc
module load pfft/1.0.8-alpha
module list
module load git
module load autotools

#autoreconf -i
make distclean || echo 'make distclean skipped'
git reset --hard
git pull
git checkout develop
git reset --hard
autoreconf -i 
automake --add-missing

export INSTALL_PREFIX=$(pwd)

export GITVERS=`git rev-parse --short=8 HEAD`
export DATE=`date +%F`
export OCTVERS=git-${GITVERS}-${DATE}
export INSTALLDIR=$INSTALL_PREFIX/octopus/$OCTVERS/intel-17.0/impi-2017.4


export MKL="-L$MKL_HOME/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm"

#export PFFT_HOME=$INSTALL_PREFIX/pfft/1.0.7a/intel-17.0/impi-2017.3
#export FFTW4PFFT_HOME=$INSTALL_PREFIX/pfft/fftw4pfft/3.3.4/intel-17.0/impi-2017.3
export ISF_HOME=$INSTALL_PREFIX/bigdft/1.7.7/intel-17.0/impi-2017.3
export METIS_HOME=$INSTALL_PREFIX/metis/5.1.0/intel-17.0
export PARMETIS_HOME=$INSTALL_PREFIX/parmetis/4.0.3/intel-17.0/impi-2017.3

./configure \
CC=mpiicc CFLAGS="-O3 -xCORE-AVX2 -fma -ip -qopenmp -fzero-initialized-in-bss" \
CXX=mpiicpc CXXFLAGS="-O3 -xCORE-AVX2 -fma -ip -qopenmp -fzero-initialized-in-bss" \
FC=mpiifort FCFLAGS="-O3 -xCORE-AVX2 -fma -ip -qopenmp -fzero-initialized-in-bss -init=arrays -init=zero" \
LDFLAGS="-L$MKL_HOME/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm -Xlinker -rpath=$MKL_HOME/lib/intel64:$GSL_HOME/lib:$PFFT_HOME/lib:$NETCDF_HOME/lib" \
FCCPP="cpp -ffreestanding" \
--prefix=$INSTALLDIR \
--enable-mpi --enable-openmp --enable-avx2 \
--disable-gdlib \
--with-gsl-prefix="$GSL_HOME" \
--with-libxc-prefix="$LIBXC_HOME" \
--with-fftw-prefix="$FFTW_HOME" \
--with-blas="$MKL" \
--with-lapack="$MKL" \
--with-blacs="$MKL" \
--with-scalapack="$MKL" \
--with-netcdf-prefix="$NETCDF_HOME" \
--with-isf-prefix="$ISF_HOME" \
--with-metis-prefix="$METIS_HOME" \
--with-parmetis-prefix="$PARMETIS_HOME"
#mismatch of MKL-FFTs and FFTW form PFFT causes segfaults: --with-pfft-prefix="$PFFT_HOME" \

make -j 

make install 

mkdir -p $INSTALLDIR/.build.doc/
cp -f config.log $INSTALLDIR/.build.doc/
cp -f $0 $INSTALLDIR/.build.doc/

echo "... done"
